// SPDX-License-Identifier: GPL-2.0-or-later

#ifndef LUPIO_SYS_H
#define LUPIO_SYS_H

#include "exec/hwaddr.h"
#include "hw/qdev-core.h"
#include "qemu/typedefs.h"

DeviceState *lupio_sys_create(hwaddr addr);

#endif /* LUPIO_SYS_H */
