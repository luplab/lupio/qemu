// SPDX-License-Identifier: GPL-2.0-or-later

#ifndef LUPIO_IPI_H
#define LUPIO_IPI_H

#include "exec/hwaddr.h"
#include "hw/qdev-core.h"
#include "qemu/typedefs.h"

DeviceState *lupio_ipi_create(hwaddr addr, uint32_t ncpus, qemu_irq *irq);

#endif /* LUPIO_IPI_H */
