// SPDX-License-Identifier: GPL-2.0-or-later

#ifndef LUPIO_RTC_H
#define LUPIO_RTC_H

#include "exec/hwaddr.h"
#include "hw/qdev-core.h"
#include "qemu/typedefs.h"

DeviceState *lupio_rtc_create(hwaddr addr);

#endif /* LUPIO_RTC_H */
