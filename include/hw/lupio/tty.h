// SPDX-License-Identifier: GPL-2.0-or-later

#ifndef LUPIO_TTY_H
#define LUPIO_TTY_H

#include "chardev/char.h"
#include "exec/hwaddr.h"
#include "hw/qdev-core.h"
#include "qemu/typedefs.h"

DeviceState *lupio_tty_create(hwaddr addr, qemu_irq irq, Chardev *chr);

#endif /* LUPIO_TTY_H */
