// SPDX-License-Identifier: GPL-2.0-or-later

#ifndef LUPIO_BLK_H
#define LUPIO_BLK_H

#include "exec/hwaddr.h"
#include "hw/qdev-core.h"
#include "qemu/typedefs.h"

DeviceState *lupio_blk_create(hwaddr addr, qemu_irq irq, DriveInfo *drv);

#endif /* LUPIO_BLK_H */
