// SPDX-License-Identifier: GPL-2.0-or-later

#ifndef LUPIO_RNG_H
#define LUPIO_RNG_H

#include "exec/hwaddr.h"
#include "hw/qdev-core.h"
#include "qemu/typedefs.h"

DeviceState *lupio_rng_create(hwaddr addr, qemu_irq irq);

#endif /* LUPIO_RNG_H */
