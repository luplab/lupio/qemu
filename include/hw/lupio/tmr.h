// SPDX-License-Identifier: GPL-2.0-or-later

#ifndef LUPIO_TMR_H
#define LUPIO_TMR_H

#include "exec/hwaddr.h"
#include "hw/qdev-core.h"
#include "qemu/typedefs.h"

DeviceState *lupio_tmr_create(hwaddr addr, uint32_t ncpus, qemu_irq *irq,
                              uint32_t freq);

#endif /* LUPIO_TMR_H */
