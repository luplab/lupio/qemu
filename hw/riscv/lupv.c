// SPDX-License-Identifier: GPL-2.0-or-later

/*
 * QEMU LupV (RISC-V+LupIO) board
 *
 * Copyright (c) 2020-2021 Joël Porquet-Lupine
 */

#include "qemu/osdep.h"
#include "hw/boards.h"
#include "hw/intc/riscv_aclint.h"
#include "hw/intc/sifive_plic.h"
#include "hw/lupio/blk.h"
#include "hw/lupio/ipi.h"
#include "hw/lupio/pic.h"
#include "hw/lupio/rng.h"
#include "hw/lupio/rtc.h"
#include "hw/lupio/sys.h"
#include "hw/lupio/tmr.h"
#include "hw/lupio/tty.h"
#include "hw/qdev-core.h"
#include "hw/qdev-properties.h"
#include "hw/riscv/boot.h"
#include "hw/riscv/riscv_hart.h"
#include "hw/sysbus.h"
#include "qapi/error.h"
#include "qemu/error-report.h"
#include "qemu/module.h"
#include "qemu/typedefs.h"
#include "qom/object.h"
#include "sysemu/arch_init.h"
#include "sysemu/device_tree.h"
#include "sysemu/sysemu.h"
#include "target/riscv/cpu.h"

#define TIMEBASE_FREQ 10000000 // 10 MHz

#define PLIC_HART_CONFIG "M"
#define PLIC_NUM_SOURCES 127
#define PLIC_NUM_PRIORITIES 7
#define PLIC_PRIORITY_BASE 0x04
#define PLIC_PENDING_BASE 0x1000
#define PLIC_ENABLE_BASE 0x2000
#define PLIC_ENABLE_STRIDE 0x80
#define PLIC_CONTEXT_BASE 0x200000
#define PLIC_CONTEXT_STRIDE 0x1000

/* Memory mapping */
enum {
    /* Memory banks */
    MEM_MROM, MEM_DRAM,
    /* Original devices */
    DEV_CLINT, DEV_PLIC,
    /* MMIO LupIO devices */
    DEV_BLK, DEV_IPI, DEV_PIC, DEV_RNG,
    DEV_RTC, DEV_SYS, DEV_TMR, DEV_TTY
};

static const struct MemmapEntry {
    hwaddr base;
    hwaddr size;
} memmap[] = {
    /* Memory regions */
    [MEM_MROM] =        { 0x00001000,      0xf000 },
    [MEM_DRAM] =        { 0x80000000,         0x0 },
    /* MMIO device regions */
    [DEV_CLINT]     =   { 0x02000000,     0x10000 },
    [DEV_PLIC]      =   { 0x0c000000,   0x4000000 },

    /* MMIO LupIO device regions */
    [DEV_BLK] = { 0x20000000, 0x1000 },
    [DEV_IPI] = { 0x20001000, 0x1000 },
    [DEV_PIC] = { 0x20002000, 0x1000 },
    [DEV_RNG] = { 0x20003000, 0x1000 },
    [DEV_RTC] = { 0x20004000, 0x1000 },
    [DEV_SYS] = { 0x20005000, 0x1000 },
    [DEV_TMR] = { 0x20006000, 0x1000 },
    [DEV_TTY] = { 0x20007000, 0x1000 },
};

/* IRQ mapping */
enum {
    TTY_IRQ,
    BLK_IRQ,
    RNG_IRQ,
};

/* Dynamic FDT generation */
static void *create_fdt(MachineState *machine, RISCVHartArrayState *cpu)
{
    int fdt_size;
    void *fdt;
    int hart;
    uint32_t *cells;
    char *nodename;
    uint32_t clk_phandle, pic_phandle, plic_phandle, sys_phandle;

    fdt = create_device_tree(&fdt_size);
    if (!fdt) {
        error_report("create_device_tree() failed");
        exit(1);
    }

    /* Root node */
    qemu_fdt_setprop_string(fdt, "/", "model", "LupLab LupV");
    qemu_fdt_setprop_string(fdt, "/", "compatible", "luplab,lupv");
    qemu_fdt_setprop_cell(fdt, "/", "#size-cells", 0x2);
    qemu_fdt_setprop_cell(fdt, "/", "#address-cells", 0x2);

    /* MMIO bus for I/O devices (range covers entire address space) */
    qemu_fdt_add_subnode(fdt, "/soc");
    qemu_fdt_setprop(fdt, "/soc", "ranges", NULL, 0);
    qemu_fdt_setprop_string(fdt, "/soc", "compatible", "simple-bus");
    qemu_fdt_setprop_cell(fdt, "/soc", "#size-cells", 0x2);
    qemu_fdt_setprop_cell(fdt, "/soc", "#address-cells", 0x2);

    /* Clock */
    clk_phandle = qemu_fdt_alloc_phandle(fdt);
    qemu_fdt_add_subnode(fdt, "/lupv-clk");
    qemu_fdt_setprop_string(fdt, "/lupv-clk", "compatible", "fixed-clock");
    qemu_fdt_setprop_cell(fdt, "/lupv-clk", "#clock-cells", 0x0);
    qemu_fdt_setprop_cell(fdt, "/lupv-clk", "clock-frequency", TIMEBASE_FREQ);
    qemu_fdt_setprop_cell(fdt, "/lupv-clk", "phandle", clk_phandle);

    /* Memory */
    nodename = g_strdup_printf("/memory@%" PRIx64, memmap[MEM_DRAM].base);
    qemu_fdt_add_subnode(fdt, nodename);
    qemu_fdt_setprop_cells(fdt, nodename, "reg",
                           memmap[MEM_DRAM].base >> 32, memmap[MEM_DRAM].base,
                           machine->ram_size >> 32, machine->ram_size);
    qemu_fdt_setprop_string(fdt, nodename, "device_type", "memory");
    g_free(nodename);

    /* CPUs */
    qemu_fdt_add_subnode(fdt, "/cpus");
    qemu_fdt_setprop_cell(fdt, "/cpus", "timebase-frequency", TIMEBASE_FREQ);
    qemu_fdt_setprop_cell(fdt, "/cpus", "#size-cells", 0x0);
    qemu_fdt_setprop_cell(fdt, "/cpus", "#address-cells", 0x1);

    for (hart = cpu->num_harts - 1; hart >= 0; hart--) {
        /* Hart's general description */
        nodename = g_strdup_printf("/cpus/cpu@%d", hart);
        char *isa = riscv_isa_string(&cpu->harts[hart]);
        char *mmu = g_strdup_printf("riscv,sv%d", TARGET_VIRT_ADDR_SPACE_BITS);

        qemu_fdt_add_subnode(fdt, nodename);
        qemu_fdt_setprop_string(fdt, nodename, "device_type", "cpu");
        qemu_fdt_setprop_string(fdt, nodename, "compatible", "riscv");
        qemu_fdt_setprop_string(fdt, nodename, "riscv,isa", isa);
        qemu_fdt_setprop_string(fdt, nodename, "mmu-type", mmu);
        qemu_fdt_setprop_string(fdt, nodename, "status", "okay");
        qemu_fdt_setprop_cell(fdt, nodename, "reg", hart);

        /* Hart's interrupt controller */
        char *intc = g_strdup_printf("/cpus/cpu@%d/interrupt-controller", hart);
        int intc_phandle = qemu_fdt_alloc_phandle(fdt);
        qemu_fdt_add_subnode(fdt, intc);
        qemu_fdt_setprop_string(fdt, intc, "compatible", "riscv,cpu-intc");
        qemu_fdt_setprop(fdt, intc, "interrupt-controller", NULL, 0);
        qemu_fdt_setprop_cell(fdt, intc, "#interrupt-cells", 1);
        qemu_fdt_setprop_cell(fdt, intc, "phandle", intc_phandle);

        g_free(mmu);
        g_free(isa);
        g_free(intc);
        g_free(nodename);
    }

    /* CLINT attached to each hart's MSI and MTI pins */
    cells =  g_new0(uint32_t, cpu->num_harts * 4);
    for (hart = 0; hart < cpu->num_harts; hart++) {
        nodename = g_strdup_printf("/cpus/cpu@%d/interrupt-controller", hart);
        uint32_t intc_phandle = qemu_fdt_get_phandle(fdt, nodename);
        cells[hart * 4 + 0] = cpu_to_be32(intc_phandle);
        cells[hart * 4 + 1] = cpu_to_be32(IRQ_M_SOFT);
        cells[hart * 4 + 2] = cpu_to_be32(intc_phandle);
        cells[hart * 4 + 3] = cpu_to_be32(IRQ_M_TIMER);
        g_free(nodename);
    }
    nodename = g_strdup_printf("/soc/clint@%" PRIx64,
                               memmap[DEV_CLINT].base);
    qemu_fdt_add_subnode(fdt, nodename);
    qemu_fdt_setprop_string(fdt, nodename, "compatible", "riscv,clint0");
    qemu_fdt_setprop_cells(fdt, nodename, "reg",
                           0x0, memmap[DEV_CLINT].base,
                           0x0, memmap[DEV_CLINT].size);
    qemu_fdt_setprop(fdt, nodename, "interrupts-extended",
                     cells, cpu->num_harts * sizeof(uint32_t) * 4);
    qemu_fdt_setprop_string(fdt, nodename, "status", "disabled");
    g_free(cells);
    g_free(nodename);

    /* PLIC attached to each hart's MEI pin */
    plic_phandle = qemu_fdt_alloc_phandle(fdt);
    cells =  g_new0(uint32_t, cpu->num_harts * 2);
    for (hart = 0; hart < cpu->num_harts; hart++) {
        nodename = g_strdup_printf("/cpus/cpu@%d/interrupt-controller", hart);
        uint32_t intc_phandle = qemu_fdt_get_phandle(fdt, nodename);
        cells[hart * 2 + 0] = cpu_to_be32(intc_phandle);
        cells[hart * 2 + 1] = cpu_to_be32(IRQ_M_EXT);
        g_free(nodename);
    }
    nodename = g_strdup_printf("/soc/plic@%" PRIx64,
                               memmap[DEV_PLIC].base);
    qemu_fdt_add_subnode(fdt, nodename);
    qemu_fdt_setprop_string(fdt, nodename, "compatible", "riscv,plic0");
    qemu_fdt_setprop(fdt, nodename, "interrupt-controller", NULL, 0);
    qemu_fdt_setprop_cell(fdt, nodename, "#interrupt-cells", 0x1);
    qemu_fdt_setprop(fdt, nodename, "interrupts-extended",
                     cells, cpu->num_harts * sizeof(uint32_t) * 2);
    qemu_fdt_setprop_cells(fdt, nodename, "reg",
                           0x0, memmap[DEV_PLIC].base,
                           0x0, memmap[DEV_PLIC].size);
    qemu_fdt_setprop_cell(fdt, nodename, "riscv,ndev", 0);
    qemu_fdt_setprop_cell(fdt, nodename, "phandle", plic_phandle);
    qemu_fdt_setprop_string(fdt, nodename, "status", "disabled");
    g_free(cells);
    g_free(nodename);

    /* TMR attached to each CPU's STI pin */
    cells =  g_new0(uint32_t, cpu->num_harts * 2);
    for (hart = 0; hart < cpu->num_harts; hart++) {
        nodename = g_strdup_printf("/cpus/cpu@%d/interrupt-controller", hart);
        uint32_t intc_phandle = qemu_fdt_get_phandle(fdt, nodename);
        cells[hart * 2 + 0] = cpu_to_be32(intc_phandle);
        cells[hart * 2 + 1] = cpu_to_be32(IRQ_S_TIMER);
        g_free(nodename);
    }
    nodename = g_strdup_printf("/soc/lupio-tmr@%" PRIx64,
                               memmap[DEV_TMR].base);
    qemu_fdt_add_subnode(fdt, nodename);
    qemu_fdt_setprop_string(fdt, nodename, "compatible", "lupio,tmr");
    qemu_fdt_setprop_cells(fdt, nodename, "reg",
                           0x0, memmap[DEV_TMR].base,
                           0x0, memmap[DEV_TMR].size);
    qemu_fdt_setprop(fdt, nodename, "interrupts-extended",
                     cells, cpu->num_harts * sizeof(uint32_t) * 2);
    qemu_fdt_setprop_cell(fdt, nodename, "clocks", clk_phandle);
    g_free(cells);
    g_free(nodename);

    /* PIC attached to each CPU's SEI pin */
    pic_phandle = qemu_fdt_alloc_phandle(fdt);
    cells =  g_new0(uint32_t, cpu->num_harts * 2);
    for (hart = 0; hart < cpu->num_harts; hart++) {
        nodename = g_strdup_printf("/cpus/cpu@%d/interrupt-controller", hart);
        uint32_t intc_phandle = qemu_fdt_get_phandle(fdt, nodename);
        cells[hart * 2 + 0] = cpu_to_be32(intc_phandle);
        cells[hart * 2 + 1] = cpu_to_be32(IRQ_S_EXT);
        g_free(nodename);
    }
    nodename = g_strdup_printf("/soc/lupio-pic@%" PRIx64,
                               memmap[DEV_PIC].base);
    qemu_fdt_add_subnode(fdt, nodename);
    qemu_fdt_setprop_string(fdt, nodename, "compatible", "lupio,pic");
    qemu_fdt_setprop(fdt, nodename, "interrupt-controller", NULL, 0);
    qemu_fdt_setprop_cell(fdt, nodename, "#interrupt-cells", 0x1);
    qemu_fdt_setprop(fdt, nodename, "interrupts-extended",
                     cells, cpu->num_harts * sizeof(uint32_t) * 2);
    qemu_fdt_setprop_cells(fdt, nodename, "reg",
                           0x0, memmap[DEV_PIC].base,
                           0x0, memmap[DEV_PIC].size);
    qemu_fdt_setprop_cell(fdt, nodename, "phandle", pic_phandle);
    g_free(cells);
    g_free(nodename);

    /* IPI attached to each CPU's SSI pin */
    cells =  g_new0(uint32_t, cpu->num_harts * 2);
    for (hart = 0; hart < cpu->num_harts; hart++) {
        nodename = g_strdup_printf("/cpus/cpu@%d/interrupt-controller", hart);
        uint32_t intc_phandle = qemu_fdt_get_phandle(fdt, nodename);
        cells[hart * 2 + 0] = cpu_to_be32(intc_phandle);
        cells[hart * 2 + 1] = cpu_to_be32(IRQ_S_SOFT);
        g_free(nodename);
    }
    nodename = g_strdup_printf("/soc/lupio-ipi@%" PRIx64,
                               memmap[DEV_IPI].base);
    qemu_fdt_add_subnode(fdt, nodename);
    qemu_fdt_setprop_string(fdt, nodename, "compatible", "lupio,ipi");
    /* It's not really an interrupt-controller, but otherwise Linux won't
     * consider it in irqchip_init() */
    qemu_fdt_setprop(fdt, nodename, "interrupt-controller", NULL, 0);
    qemu_fdt_setprop_cells(fdt, nodename, "reg",
                           0x0, memmap[DEV_IPI].base,
                           0x0, memmap[DEV_IPI].size);
    qemu_fdt_setprop(fdt, nodename, "interrupts-extended",
                     cells, cpu->num_harts * sizeof(uint32_t) * 2);
    g_free(cells);
    g_free(nodename);

    /* System controller */
    sys_phandle = qemu_fdt_alloc_phandle(fdt);
    nodename = g_strdup_printf("/soc/lupio-sys@%" PRIx64,
                               memmap[DEV_SYS].base);
    qemu_fdt_add_subnode(fdt, nodename);
    qemu_fdt_setprop_string(fdt, nodename, "compatible", "syscon");
    qemu_fdt_setprop_cells(fdt, nodename, "reg",
                           0x0, memmap[DEV_SYS].base,
                           0x0, memmap[DEV_SYS].size);
    qemu_fdt_setprop_cell(fdt, nodename, "phandle", sys_phandle);
    g_free(nodename);

    nodename = g_strdup_printf("/soc/poweroff");
    qemu_fdt_add_subnode(fdt, nodename);
    qemu_fdt_setprop_string(fdt, nodename, "compatible", "syscon-poweroff");
    qemu_fdt_setprop_cell(fdt, nodename, "regmap", sys_phandle);
    qemu_fdt_setprop_cell(fdt, nodename, "offset", 0x0);
    qemu_fdt_setprop_cell(fdt, nodename, "value", 1);
    g_free(nodename);

    nodename = g_strdup_printf("/soc/reboot");
    qemu_fdt_add_subnode(fdt, nodename);
    qemu_fdt_setprop_string(fdt, nodename, "compatible", "syscon-reboot");
    qemu_fdt_setprop_cell(fdt, nodename, "regmap", sys_phandle);
    qemu_fdt_setprop_cell(fdt, nodename, "offset", 0x4);
    qemu_fdt_setprop_cell(fdt, nodename, "value", 1);
    g_free(nodename);

    /* TTY */
    nodename = g_strdup_printf("/soc/lupio-tty@%" PRIx64,
                               memmap[DEV_TTY].base);
    qemu_fdt_add_subnode(fdt, nodename);
    qemu_fdt_setprop_string(fdt, nodename, "compatible", "lupio,tty");
    qemu_fdt_setprop_cells(fdt, nodename, "reg",
                           0x0, memmap[DEV_TTY].base,
                           0x0, memmap[DEV_TTY].size);
    qemu_fdt_setprop_cell(fdt, nodename, "interrupt-parent", pic_phandle);
    qemu_fdt_setprop_cell(fdt, nodename, "interrupts", TTY_IRQ);

    /* Complete chosen node based on TTY's path */
    qemu_fdt_add_subnode(fdt, "/chosen");
    qemu_fdt_setprop_string(fdt, "/chosen", "stdout-path", nodename);
    g_free(nodename);

    /* Block device */
    nodename = g_strdup_printf("/soc/lupio-blk@%" PRIx64,
                               memmap[DEV_BLK].base);
    qemu_fdt_add_subnode(fdt, nodename);
    qemu_fdt_setprop_string(fdt, nodename, "compatible", "lupio,blk");
    qemu_fdt_setprop_cells(fdt, nodename, "reg",
                           0x0, memmap[DEV_BLK].base,
                           0x0, memmap[DEV_BLK].size);
    qemu_fdt_setprop_cell(fdt, nodename, "interrupt-parent", pic_phandle);
    qemu_fdt_setprop_cell(fdt, nodename, "interrupts", BLK_IRQ);
    g_free(nodename);

    /* Real time clock */
    nodename = g_strdup_printf("/soc/lupio-rtc@%" PRIx64,
                               memmap[DEV_RTC].base);
    qemu_fdt_add_subnode(fdt, nodename);
    qemu_fdt_setprop_string(fdt, nodename, "compatible", "lupio,rtc");
    qemu_fdt_setprop_cells(fdt, nodename, "reg",
                           0x0, memmap[DEV_RTC].base,
                           0x0, memmap[DEV_RTC].size);
    g_free(nodename);

    /* Random number generator */
    nodename = g_strdup_printf("/soc/lupio-rng@%" PRIx64,
                               memmap[DEV_RNG].base);
    qemu_fdt_add_subnode(fdt, nodename);
    qemu_fdt_setprop_string(fdt, nodename, "compatible", "lupio,rng");
    qemu_fdt_setprop_cells(fdt, nodename, "reg",
                           0x0, memmap[DEV_RNG].base,
                           0x0, memmap[DEV_RNG].size);
    qemu_fdt_setprop_cell(fdt, nodename, "interrupt-parent", pic_phandle);
    qemu_fdt_setprop_cell(fdt, nodename, "interrupts", RNG_IRQ);
    g_free(nodename);

    return fdt;
}

static void lupv_init(MachineState *machine)
{
    RISCVHartArrayState *cpu = g_new0(RISCVHartArrayState, 1);
    MemoryRegion *system_memory = get_system_memory();
    MemoryRegion *main_mem = g_new(MemoryRegion, 1);
    MemoryRegion *mask_rom = g_new(MemoryRegion, 1);
    DeviceState *pic;
    void *fdt;
    target_ulong firmware_end_addr, kernel_start_addr;
    uint32_t fdt_load_addr;
    uint64_t kernel_entry = 0;
    int i;
    unsigned int smp_cpus = machine->smp.cpus;
    qemu_irq *cpu_ssip_irqs, *cpu_stip_irqs, *cpu_seip_irqs;
    char *plic_hart_config;
    size_t plic_hart_config_len;

    /* CPU subsystem */
    object_initialize_child(OBJECT(machine), "cpu", cpu, TYPE_RISCV_HART_ARRAY);
    object_property_set_str(OBJECT(cpu), "cpu-type", machine->cpu_type, &error_abort);
    object_property_set_int(OBJECT(cpu), "num-harts", smp_cpus, &error_abort);
    sysbus_realize(SYS_BUS_DEVICE(cpu), &error_abort);

    /* Memory subsystem */
    memory_region_init_ram(main_mem, NULL, "riscv_lupv_board.ram",
                           machine->ram_size, &error_fatal);
    memory_region_add_subregion(system_memory, memmap[MEM_DRAM].base, main_mem);

    memory_region_init_rom(mask_rom, NULL, "riscv_lupv_board.mrom",
                           memmap[MEM_MROM].size, &error_fatal);
    memory_region_add_subregion(system_memory, memmap[MEM_MROM].base, mask_rom);

    /* IRQ subsystem */
    cpu_ssip_irqs = g_malloc_n(smp_cpus, sizeof(qemu_irq));
    cpu_stip_irqs = g_malloc_n(smp_cpus, sizeof(qemu_irq));
    cpu_seip_irqs = g_malloc_n(smp_cpus, sizeof(qemu_irq));

    for (i = 0; i < smp_cpus; i++) {
        cpu_ssip_irqs[i] = qdev_get_gpio_in(DEVICE(&cpu->harts[i]), IRQ_S_SOFT);
        cpu_stip_irqs[i] = qdev_get_gpio_in(DEVICE(&cpu->harts[i]), IRQ_S_TIMER);
        cpu_seip_irqs[i] = qdev_get_gpio_in(DEVICE(&cpu->harts[i]), IRQ_S_EXT);
    }

    /* Create PLIC hart topology configuration string */
    plic_hart_config_len = (strlen(PLIC_HART_CONFIG) + 1) * smp_cpus;
    plic_hart_config = g_malloc0(plic_hart_config_len);
    for (i = 0; i < smp_cpus; i++) {
        if (i != 0) {
            strncat(plic_hart_config, ",", plic_hart_config_len);
        }
        strncat(plic_hart_config, PLIC_HART_CONFIG, plic_hart_config_len);
        plic_hart_config_len -= (strlen(PLIC_HART_CONFIG) + 1);
    }

    /* MMIO subsystem */
    sifive_plic_create(memmap[DEV_PLIC].base,
                              plic_hart_config, smp_cpus, 0,
                              PLIC_NUM_SOURCES,
                              PLIC_NUM_PRIORITIES,
                              PLIC_PRIORITY_BASE,
                              PLIC_PENDING_BASE,
                              PLIC_ENABLE_BASE,
                              PLIC_ENABLE_STRIDE,
                              PLIC_CONTEXT_BASE,
                              PLIC_CONTEXT_STRIDE,
                              memmap[DEV_PLIC].size);

    riscv_aclint_swi_create(memmap[DEV_CLINT].base,
                            0, smp_cpus, false);
    riscv_aclint_mtimer_create(memmap[DEV_CLINT].base + RISCV_ACLINT_SWI_SIZE,
                               RISCV_ACLINT_DEFAULT_MTIMER_SIZE, 0, smp_cpus,
                               RISCV_ACLINT_DEFAULT_MTIMECMP,
                               RISCV_ACLINT_DEFAULT_MTIME,
                               RISCV_ACLINT_DEFAULT_TIMEBASE_FREQ, false);

    pic = lupio_pic_create(memmap[DEV_PIC].base, smp_cpus, cpu_seip_irqs);

    lupio_tmr_create(memmap[DEV_TMR].base, smp_cpus, cpu_stip_irqs,
                     TIMEBASE_FREQ);

    lupio_ipi_create(memmap[DEV_IPI].base, smp_cpus, cpu_ssip_irqs);

    lupio_blk_create(memmap[DEV_BLK].base,
                     qdev_get_gpio_in(DEVICE(pic), BLK_IRQ),
                     drive_get(IF_NONE, 0, 0));

    lupio_rtc_create(memmap[DEV_RTC].base);

    lupio_rng_create(memmap[DEV_RNG].base, qdev_get_gpio_in(DEVICE(pic), RNG_IRQ));

    lupio_sys_create(memmap[DEV_SYS].base);

    lupio_tty_create(memmap[DEV_TTY].base,
                     qdev_get_gpio_in(DEVICE(pic), TTY_IRQ),
                     serial_hd(0));

    g_free(plic_hart_config);

    g_free(cpu_ssip_irqs);
    g_free(cpu_stip_irqs);
    g_free(cpu_seip_irqs);

    /* FDT generation */
    fdt = create_fdt(machine, cpu);

    /* Load BIOS */
    firmware_end_addr = riscv_find_and_load_firmware(machine, NULL,
                                                     memmap[MEM_DRAM].base,
                                                     NULL);

    /* Load kernel */
    if (machine->kernel_filename) {
        kernel_start_addr = riscv_calc_kernel_start_addr(cpu,
                                                         firmware_end_addr);

        /* Kernel image */
        kernel_entry = riscv_load_kernel(machine->kernel_filename,
                                         kernel_start_addr, NULL);
        qemu_fdt_setprop_cell(fdt, "/chosen", "kernel-start", kernel_entry);

        /* Initrd image */
        if (machine->initrd_filename) {
            hwaddr start;
            hwaddr end = riscv_load_initrd(machine->initrd_filename,
                                           machine->ram_size, kernel_entry,
                                           &start);
            qemu_fdt_setprop_cell(fdt, "/chosen", "linux,initrd-start", start);
            qemu_fdt_setprop_cell(fdt, "/chosen", "linux,initrd-end", end);
        }
    }
    if (machine->kernel_cmdline)
        qemu_fdt_setprop_string(fdt, "/chosen", "bootargs",
                                machine->kernel_cmdline);

    /* Load FDT in RAM */
    fdt_load_addr = riscv_load_fdt(memmap[MEM_DRAM].base,
                                   machine->ram_size, fdt);

    /* Load reset vector in MROM */
    riscv_setup_rom_reset_vec(machine, cpu, memmap[MEM_DRAM].base,
                              memmap[MEM_MROM].base,
                              memmap[MEM_MROM].size,
                              kernel_entry, fdt_load_addr, fdt);
}

static void lupv_machine_init(MachineClass *mc)
{
    mc->desc = "LupV (RISC-V+LupIO) board";
    mc->init = lupv_init;
    mc->max_cpus = 8;
    mc->default_cpu_type = TYPE_RISCV_CPU_BASE;
}

DEFINE_MACHINE("lupv", lupv_machine_init)
