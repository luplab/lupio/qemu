// SPDX-License-Identifier: GPL-2.0-or-later

/*
 * QEMU LupA64 (ARM64+LupIO) board
 *
 * Copyright (c) 2021 Joël Porquet-Lupine, Laura Hinman, Madison Brigham
 *
 * Inspired by Xilinx Versal Virtual board.
 * Copyright (c) 2018 Xilinx Inc.
 * Written by Edgar E. Iglesias
 */

#include "qemu/osdep.h"
#include "qapi/error.h"

#include "exec/address-spaces.h"
#include "hw/arm/boot.h"
#include "hw/arm/fdt.h"
#include "hw/boards.h"
#include "hw/intc/arm_gicv3.h"
#include "hw/lupio/blk.h"
#include "hw/lupio/rtc.h"
#include "hw/lupio/rng.h"
#include "hw/lupio/sys.h"
#include "hw/lupio/tty.h"
#include "hw/qdev-core.h"
#include "hw/qdev-properties.h"
#include "hw/sysbus.h"
#include "qemu/error-report.h"
#include "qemu/module.h"
#include "qemu/typedefs.h"
#include "qemu/units.h"
#include "qom/object.h"
#include "sysemu/device_tree.h"
#include "sysemu/sysemu.h"
#include "target/arm/cpu.h"
#include "target/arm/kvm_arm.h"

#define TIMEBASE_FREQ 125000000 // 125 MHz

#define MAX_CPUS 8

/* Number of external interrupt lines to configure the GIC with */
#define NUM_IRQS 256

/* IRQ map */
#define LUPA64_GIC_MAINT_IRQ    9
#define LUPA64_TIMER_VIRT_IRQ   11
#define LUPA64_TIMER_S_EL1_IRQ  13
#define LUPA64_TIMER_NS_EL1_IRQ 14
#define LUPA64_TIMER_NS_EL2_IRQ 10
#define LUPA64_BLK_IRQ          3
#define LUPA64_RNG_IRQ          2
#define LUPA64_TTY_IRQ          0


enum {
    /* Memory bank */
    MEM_DRAM,
    /* ARM core devices */
    DEV_GIC_DIST, DEV_GIC_REDIST,
    /* LupIO peripheral devices */
    DEV_BLK, DEV_RNG, DEV_RTC, DEV_SYS, DEV_TTY,
};

/* Memory map */
static const MemMapEntry memmap[] = {
    /* Memory region(s) */
    [MEM_DRAM]          = { 0x00000000, 0x00000000 },

    /* MMIO ARM device regions */
    [DEV_GIC_DIST]      = { 0xf9000000, 0x00010000 },
    [DEV_GIC_REDIST]    = { 0xf9080000, 0x00080000 },

    /* MMIO LupIO device regions */
    [DEV_BLK]           = { 0xfc000000, 0x00001000 },
    [DEV_RNG]           = { 0xfc003000, 0x00001000 },
    [DEV_RTC]           = { 0xfc004000, 0x00001000 },
    [DEV_SYS]           = { 0xfc005000, 0x00001000 },
    [DEV_TTY]           = { 0xfc007000, 0x00001000 },
};

#define TYPE_LUPA64_MACHINE MACHINE_TYPE_NAME("lupA64")
OBJECT_DECLARE_SIMPLE_TYPE(LupA64MachineState, LUPA64_MACHINE)

struct LupA64MachineState {
    MachineState parent;
    int psci_conduit;
    struct arm_boot_info binfo;
};

static void *lupa64_get_dtb(const struct arm_boot_info *info, int *size)
{
    const LupA64MachineState *lms = container_of(info, LupA64MachineState, binfo);
    const MachineState *ms = MACHINE(lms);

    int fdt_size;
    void *fdt;
    char *nodename;
    uint32_t phandle_gic, phandle_clk, phandle_sys;
    int i, smp_cpus = ms->smp.cpus;

    fdt = create_device_tree(&fdt_size);
    if (!fdt) {
        error_report("create_device_tree() failed");
        exit(1);
    }

    /* Allocate all phandles.  */
    phandle_clk = qemu_fdt_alloc_phandle(fdt);
    phandle_gic = qemu_fdt_alloc_phandle(fdt);
    phandle_sys = qemu_fdt_alloc_phandle(fdt);

    /* Header */
    qemu_fdt_setprop_string(fdt, "/", "model", "LupLab LupA64");
    qemu_fdt_setprop_string(fdt, "/", "compatible", "luplab,lupa64");
    qemu_fdt_setprop_cell(fdt, "/", "#size-cells", 0x2);
    qemu_fdt_setprop_cell(fdt, "/", "#address-cells", 0x2);
    qemu_fdt_setprop_cell(fdt, "/", "interrupt-parent", phandle_gic);

    /* Create /chosen node for boot console */
    qemu_fdt_add_subnode(fdt, "/chosen");

    /* Clock */
    qemu_fdt_add_subnode(fdt, "/lupa64-clk");
    qemu_fdt_setprop_string(fdt, "/lupa64-clk", "compatible", "fixed-clock");
    qemu_fdt_setprop_cell(fdt, "/lupa64-clk", "#clock-cells", 0x0);
    qemu_fdt_setprop_cell(fdt, "/lupa64-clk", "clock-frequency", TIMEBASE_FREQ);
    qemu_fdt_setprop_cell(fdt, "/lupa64-clk", "phandle", phandle_clk);

    /* Memory node is added by generic arm code already */

    /* CPUs */
    qemu_fdt_add_subnode(fdt, "/cpus");
    qemu_fdt_setprop_cell(fdt, "/cpus", "#address-cells", 0x1);
    qemu_fdt_setprop_cell(fdt, "/cpus", "#size-cells", 0x0);

    for (i = smp_cpus - 1; i >= 0; i--) {
        nodename = g_strdup_printf("/cpus/cpu@%d", i);
        ARMCPU *armcpu = ARM_CPU(qemu_get_cpu(i));
        qemu_fdt_add_subnode(fdt, nodename);
        qemu_fdt_setprop_string(fdt, nodename, "device_type", "cpu");
        qemu_fdt_setprop_string(fdt, nodename, "compatible",
                                armcpu->dtb_compatible);
        if (info->psci_conduit != QEMU_PSCI_CONDUIT_DISABLED) {
            qemu_fdt_setprop_string(fdt, nodename, "enable-method", "psci");
        }
        qemu_fdt_setprop_cell(fdt, nodename, "reg", armcpu->mp_affinity);
        g_free(nodename);
    }

    /* GIC */
    nodename = g_strdup_printf("/intc@%" PRIx64, memmap[DEV_GIC_DIST].base);
    qemu_fdt_add_subnode(fdt, nodename);
    qemu_fdt_setprop_cell(fdt, nodename, "#interrupt-cells", 3);
    qemu_fdt_setprop(fdt, nodename, "interrupt-controller", NULL, 0);
    qemu_fdt_setprop_cell(fdt, nodename, "#address-cells", 0x2);
    qemu_fdt_setprop_cell(fdt, nodename, "#size-cells", 0x2);
    qemu_fdt_setprop(fdt, nodename, "ranges", NULL, 0);
    qemu_fdt_setprop_string(fdt, nodename, "compatible", "arm,gic-v3");
    qemu_fdt_setprop_sized_cells(fdt, nodename, "reg",
                                 2, memmap[DEV_GIC_DIST].base,
                                 2, memmap[DEV_GIC_DIST].size,
                                 2, memmap[DEV_GIC_REDIST].base,
                                 2, memmap[DEV_GIC_REDIST].size);
    qemu_fdt_setprop_cell(fdt, nodename, "phandle", phandle_gic);
    g_free(nodename);

    /* Timer */
    uint32_t irqflags = GIC_FDT_IRQ_FLAGS_LEVEL_HI;

    qemu_fdt_add_subnode(fdt, "/timer");
    qemu_fdt_setprop_string(fdt, "/timer", "compatible", "arm,armv8-timer");
    qemu_fdt_setprop_cells(fdt, "/timer", "interrupts",
            GIC_FDT_IRQ_TYPE_PPI, LUPA64_TIMER_S_EL1_IRQ, irqflags,
            GIC_FDT_IRQ_TYPE_PPI, LUPA64_TIMER_NS_EL1_IRQ, irqflags,
            GIC_FDT_IRQ_TYPE_PPI, LUPA64_TIMER_VIRT_IRQ, irqflags,
            GIC_FDT_IRQ_TYPE_PPI, LUPA64_TIMER_NS_EL2_IRQ, irqflags);

    /* LupIO SYS */
    nodename = g_strdup_printf("/lupio-sys@%" PRIx64, memmap[DEV_SYS].base);
    qemu_fdt_add_subnode(fdt, nodename);
    qemu_fdt_setprop_string(fdt, nodename, "compatible", "syscon");
    qemu_fdt_setprop_sized_cells(fdt, nodename, "reg",
                                 2, memmap[DEV_SYS].base,
                                 2, memmap[DEV_SYS].size);
    qemu_fdt_setprop_cell(fdt, nodename, "phandle", phandle_sys);
    g_free(nodename);

    nodename = g_strdup_printf("/poweroff");
    qemu_fdt_add_subnode(fdt, nodename);
    qemu_fdt_setprop_string(fdt, nodename, "compatible", "syscon-poweroff");
    qemu_fdt_setprop_cell(fdt, nodename, "regmap", phandle_sys);
    qemu_fdt_setprop_cell(fdt, nodename, "offset", 0x0);
    qemu_fdt_setprop_cell(fdt, nodename, "value", 1);
    g_free(nodename);

    nodename = g_strdup_printf("/reboot");
    qemu_fdt_add_subnode(fdt, nodename);
    qemu_fdt_setprop_string(fdt, nodename, "compatible", "syscon-reboot");
    qemu_fdt_setprop_cell(fdt, nodename, "regmap", phandle_sys);
    qemu_fdt_setprop_cell(fdt, nodename, "offset", 0x4);
    qemu_fdt_setprop_cell(fdt, nodename, "value", 1);
    g_free(nodename);

    /* LupIO TTY */
    nodename = g_strdup_printf("/lupio-tty@%" PRIx64, memmap[DEV_TTY].base);
    qemu_fdt_add_subnode(fdt, nodename);
    qemu_fdt_setprop_string(fdt, nodename, "compatible", "lupio,tty");
    qemu_fdt_setprop_sized_cells(fdt, nodename, "reg",
                                 2, memmap[DEV_TTY].base,
                                 2, memmap[DEV_TTY].size);
    qemu_fdt_setprop_cells(fdt, nodename, "interrupts",
                           GIC_FDT_IRQ_TYPE_SPI, LUPA64_TTY_IRQ,
                           GIC_FDT_IRQ_FLAGS_LEVEL_HI);

    qemu_fdt_setprop_string(fdt, "/chosen", "stdout-path", nodename);
    g_free(nodename);

    /* LupIO BLK */
    nodename = g_strdup_printf("/lupio-blk@%" PRIx64, memmap[DEV_BLK].base);
    qemu_fdt_add_subnode(fdt, nodename);
    qemu_fdt_setprop_string(fdt, nodename, "compatible", "lupio,blk");
    qemu_fdt_setprop_sized_cells(fdt, nodename, "reg",
                                 2, memmap[DEV_BLK].base,
                                 2, memmap[DEV_BLK].size);
    qemu_fdt_setprop_cells(fdt, nodename, "interrupts", GIC_FDT_IRQ_TYPE_SPI,
                           LUPA64_BLK_IRQ, GIC_FDT_IRQ_FLAGS_LEVEL_HI);
    g_free(nodename);

    /* LupIO RTC */
    nodename = g_strdup_printf("/lupio-rtc@%" PRIx64, memmap[DEV_RTC].base);
    qemu_fdt_add_subnode(fdt, nodename);
    qemu_fdt_setprop_string(fdt, nodename, "compatible", "lupio,rtc");
    qemu_fdt_setprop_sized_cells(fdt, nodename, "reg",
                                 2, memmap[DEV_RTC].base,
                                 2, memmap[DEV_RTC].size);
    g_free(nodename);

    /* LupIO RNG */
    nodename = g_strdup_printf("/lupio-rng@%" PRIx64, memmap[DEV_RNG].base);
    qemu_fdt_add_subnode(fdt, nodename);
    qemu_fdt_setprop_string(fdt, nodename, "compatible", "lupio,rng");
    qemu_fdt_setprop_sized_cells(fdt, nodename, "reg",
                                 2, memmap[DEV_RNG].base,
                                 2, memmap[DEV_RNG].size);
    qemu_fdt_setprop_cells(fdt, nodename, "interrupts", GIC_FDT_IRQ_TYPE_SPI,
                           LUPA64_RNG_IRQ, GIC_FDT_IRQ_FLAGS_LEVEL_HI);
    g_free(nodename);

    *size = fdt_size;
    return fdt;
}

static void lupa64_init(MachineState *machine)
{
    LupA64MachineState *lms = LUPA64_MACHINE(machine);
    unsigned int smp_cpus = machine->smp.cpus;
    DeviceState *pic;
    SysBusDevice *sys;
    int i, psci_conduit = QEMU_PSCI_CONDUIT_DISABLED;

    if (machine->kernel_filename)
        psci_conduit = QEMU_PSCI_CONDUIT_HVC;
    /* Memory subsystem */
    memory_region_add_subregion(get_system_memory(), memmap[MEM_DRAM].base,
                                machine->ram);


    /* CPU subsystem */
    for (i = 0; i < smp_cpus; i++) {
        Object *cpuobj = object_new(machine->cpu_type);

        if (object_property_find(cpuobj, "has_el3"))
            object_property_set_bool(cpuobj, "has_el3", false, &error_fatal);
        if (object_property_find(cpuobj, "has_el2"))
            object_property_set_bool(cpuobj, "has_el2", false, &error_fatal);

        object_property_set_link(cpuobj, "memory", OBJECT(get_system_memory()),
                                 &error_abort);

        qdev_realize(DEVICE(cpuobj), NULL, &error_fatal);
    }

    /* GIC */
    pic = qdev_new(gicv3_class_name());
    sys = SYS_BUS_DEVICE(pic);
    qdev_prop_set_uint32(pic, "revision", 3);
    qdev_prop_set_uint32(pic, "num-cpu", smp_cpus);
    qdev_prop_set_uint32(pic, "num-irq", NUM_IRQS + 32);
    qdev_prop_set_uint32(pic, "len-redist-region-count", 1);
    qdev_prop_set_uint32(pic, "redist-region-count[0]", smp_cpus);
    qdev_prop_set_bit(pic, "has-security-extensions", false);

    sysbus_realize_and_unref(sys, &error_fatal);
    sysbus_mmio_map(sys, 0, memmap[DEV_GIC_DIST].base);
    sysbus_mmio_map(sys, 1, memmap[DEV_GIC_REDIST].base);

    /* Write GIC and CPUs */
    for (i = 0; i < smp_cpus; i++) {
        DeviceState *cpudev = DEVICE(qemu_get_cpu(i));
        int ppibase = NUM_IRQS + i * GIC_INTERNAL + GIC_NR_SGIS;
        int irq;

        /* Mapping from the output timer irq lines from the CPU to the
         * GIC PPI inputs */
        const int timer_irq[] = {
            [GTIMER_PHYS] = LUPA64_TIMER_NS_EL1_IRQ,
            [GTIMER_VIRT] = LUPA64_TIMER_VIRT_IRQ,
            [GTIMER_HYP]  = LUPA64_TIMER_NS_EL2_IRQ,
            [GTIMER_SEC]  = LUPA64_TIMER_S_EL1_IRQ,
        };

        /* Connect timer to GIC */
        for (irq = 0; irq < ARRAY_SIZE(timer_irq); irq++) {
            qdev_connect_gpio_out(cpudev, irq,
                                  qdev_get_gpio_in(pic,
                                                   ppibase + timer_irq[irq]));
        }

        /* Connect maintainance irq to GIC */
        qdev_connect_gpio_out_named(cpudev, "gicv3-maintenance-interrupt", 0,
                                    qdev_get_gpio_in(pic, ppibase + LUPA64_GIC_MAINT_IRQ));

        /* Connect GIC to CPU */
        sysbus_connect_irq(sys, i, qdev_get_gpio_in(cpudev, ARM_CPU_IRQ));
        sysbus_connect_irq(sys, i + smp_cpus,
                           qdev_get_gpio_in(cpudev, ARM_CPU_FIQ));
        sysbus_connect_irq(sys, i + 2 * smp_cpus,
                           qdev_get_gpio_in(cpudev, ARM_CPU_VIRQ));
        sysbus_connect_irq(sys, i + 3 * smp_cpus,
                           qdev_get_gpio_in(cpudev, ARM_CPU_VFIQ));
    }

    /* Peripheral devices */
    lupio_blk_create(memmap[DEV_BLK].base,
                     qdev_get_gpio_in(DEVICE(pic), LUPA64_BLK_IRQ),
                     drive_get(IF_NONE, 0, 0));

    lupio_rng_create(memmap[DEV_RNG].base,
                     qdev_get_gpio_in(DEVICE(pic), LUPA64_RNG_IRQ));

    lupio_rtc_create(memmap[DEV_RTC].base);

    lupio_sys_create(memmap[DEV_SYS].base);

    lupio_tty_create(memmap[DEV_TTY].base,
                     qdev_get_gpio_in(DEVICE(pic), LUPA64_TTY_IRQ),
                     serial_hd(0));

    /* Kernel and FDT loading */
    lms->binfo.loader_start = memmap[MEM_DRAM].base;
    lms->binfo.ram_size = machine->ram_size;
    lms->binfo.get_dtb = lupa64_get_dtb;
    lms->binfo.psci_conduit = psci_conduit;
    arm_load_kernel(ARM_CPU(first_cpu), machine, &lms->binfo);
}

static void lupa64_machine_class_init(ObjectClass *oc, void *data)
{
    MachineClass *mc = MACHINE_CLASS(oc);
    mc->desc = "LupA64 (ARM64+LupIO) board";
    mc->init = lupa64_init;
    mc->max_cpus = MAX_CPUS;
    mc->default_cpu_type = ARM_CPU_TYPE_NAME("cortex-a72");
    mc->default_ram_size = 2 * GiB;
    mc->default_ram_id = "lupa64.ram";
    mc->no_cdrom = true;
}

static const TypeInfo lupa64_machine_info = {
    .name          = TYPE_LUPA64_MACHINE,
    .parent        = TYPE_MACHINE,
    .instance_size = sizeof(LupA64MachineState),
    .class_init    = lupa64_machine_class_init,
};

static void lupa64_machine_register_types(void)
{
    type_register_static(&lupa64_machine_info);
}

type_init(lupa64_machine_register_types);

