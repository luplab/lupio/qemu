// SPDX-License-Identifier: GPL-2.0-or-later

/*
 * QEMU LupA32 (ARM32+LupIO) board
 *
 * Copyright (C) 2022 Joël Porquet-Lupine, Laura Hinman, Madison Brigham
 *
 * Inspired by Cubietech Cubieboard.
 * Copyright (C) 2013 Li Guang
 * Written by Li Guang <lig.fnst@cn.fujitsu.com>
 */

#include "qemu/osdep.h"
#include "qapi/error.h"

#include "exec/address-spaces.h"
#include "hw/arm/boot.h"
#include "hw/boards.h"
#include "hw/lupio/blk.h"
#include "hw/lupio/ipi.h"
#include "hw/lupio/pic.h"
#include "hw/lupio/rtc.h"
#include "hw/lupio/rng.h"
#include "hw/lupio/sys.h"
#include "hw/lupio/tmr.h"
#include "hw/lupio/tty.h"
#include "hw/qdev-core.h"
#include "hw/qdev-properties.h"
#include "hw/sysbus.h"
#include "qemu/error-report.h"
#include "qemu/module.h"
#include "qemu/typedefs.h"
#include "qemu/units.h"
#include "qom/object.h"
#include "sysemu/device_tree.h"
#include "sysemu/sysemu.h"
#include "target/arm/cpu.h"

#define TIMEBASE_FREQ  24000000 // 24 MHz

#define MAX_CPUS 8

/* IRQ map */
enum {
    /* Per-cpu IRQs */
    LUPA32_TMR_IRQ = 0,
    LUPA32_IPI_IRQ,
    LUPA32_PCPU_MAX,

    /* Shared IRQs */
    LUPA32_TTY_IRQ = LUPA32_PCPU_MAX * MAX_CPUS,
    LUPA32_BLK_IRQ,
    LUPA32_RNG_IRQ,
};

enum {
    /* Memory bank(s) */
    MEM_DRAM,
    /* LupIO core devices */
    DEV_PIC, DEV_TMR, DEV_IPI,
    /* LupIO peripheral devices */
    DEV_BLK, DEV_RNG, DEV_RTC, DEV_SYS, DEV_TTY,
};

/* Memory map */
static const MemMapEntry memmap[] = {
    /* Memory region(s) */
    [MEM_DRAM]      = { 0x40000000, 0x0},

    /* MMIO LupIO device regions */
    [DEV_BLK]       = { 0x01c30000, 0x00001000 },
    [DEV_IPI]       = { 0x01c31000, 0x00001000 },
    [DEV_PIC]       = { 0x01c32000, 0x00001000 },
    [DEV_RNG]       = { 0x01c33000, 0x00001000 },
    [DEV_RTC]       = { 0x01c34000, 0x00001000 },
    [DEV_SYS]       = { 0x01c35000, 0x00001000 },
    [DEV_TMR]       = { 0x01c36000, 0x00001000 },
    [DEV_TTY]       = { 0x01c37000, 0x00001000 },
};

#define TYPE_LUPA32_MACHINE MACHINE_TYPE_NAME("lupA32")
OBJECT_DECLARE_SIMPLE_TYPE(LupA32MachineState, LUPA32_MACHINE)

struct LupA32MachineState {
    MachineState parent;
    struct arm_boot_info binfo;
};

static void *lupa32_get_dtb(const struct arm_boot_info *info, int *size)
{
    const LupA32MachineState *lms = container_of(info, LupA32MachineState,
                                                 binfo);
    const MachineState *ms = MACHINE(lms);

    int fdt_size;
    void *fdt;
    uint32_t *cells;
    char *nodename;
    uint32_t phandle_pic, phandle_clk, phandle_sys;
    int i, smp_cpus = ms->smp.cpus;

    fdt = create_device_tree(&fdt_size);
    if (!fdt) {
        error_report("create_device_tree() failed");
        exit(1);
    }

    /* Allocate all phandles */
    phandle_clk = qemu_fdt_alloc_phandle(fdt);
    phandle_pic = qemu_fdt_alloc_phandle(fdt);
    phandle_sys = qemu_fdt_alloc_phandle(fdt);

    /* Header */
    qemu_fdt_setprop_string(fdt, "/", "model", "LupLab LupA32");
    qemu_fdt_setprop_string(fdt, "/", "compatible", "luplab,lupa32");
    qemu_fdt_setprop_cell(fdt, "/", "#address-cells", 0x1);
    qemu_fdt_setprop_cell(fdt, "/", "#size-cells", 0x1);
    qemu_fdt_setprop_cell(fdt, "/", "interrupt-parent", phandle_pic);

    /* Create /chosen node for boot console */
    qemu_fdt_add_subnode(fdt, "/chosen");

    /* Clock */
    qemu_fdt_add_subnode(fdt, "/lupa32-clk");
    qemu_fdt_setprop_string(fdt, "/lupa32-clk", "compatible", "fixed-clock");
    qemu_fdt_setprop_cell(fdt, "/lupa32-clk", "#clock-cells", 0x0);
    qemu_fdt_setprop_cell(fdt, "/lupa32-clk", "clock-frequency", 24000000);
    qemu_fdt_setprop_cell(fdt, "/lupa32-clk", "phandle", phandle_clk);

    /* Memory node is added by generic arm code already */

    /* CPUs */
    qemu_fdt_add_subnode(fdt, "/cpus");
    qemu_fdt_setprop_cell(fdt, "/cpus", "#address-cells", 0x1);
    qemu_fdt_setprop_cell(fdt, "/cpus", "#size-cells", 0x0);

    for (i = smp_cpus - 1; i >= 0; i--) {
        nodename = g_strdup_printf("/cpus/cpu@%d", i);
        ARMCPU *armcpu = ARM_CPU(qemu_get_cpu(i));
        qemu_fdt_add_subnode(fdt, nodename);
        qemu_fdt_setprop_string(fdt, nodename, "device_type", "cpu");
        qemu_fdt_setprop_string(fdt, nodename, "compatible",
                                armcpu->dtb_compatible);
        qemu_fdt_setprop_string(fdt, nodename, "enable-method", "psci");
        qemu_fdt_setprop_cell(fdt, nodename, "reg", armcpu->mp_affinity);
        g_free(nodename);
    }

    qemu_fdt_add_subnode(fdt, "/soc");
    qemu_fdt_setprop_string(fdt, "/soc", "compatible", "simple-bus");
    qemu_fdt_setprop_cell(fdt, "/soc", "#address-cells", 0x1);
    qemu_fdt_setprop_cell(fdt, "/soc", "#size-cells", 0x1);
    qemu_fdt_setprop(fdt, "/soc", "ranges", NULL, 0);

    /* PIC */
    nodename = g_strdup_printf("/soc/lupio-pic@%" PRIx64,
                               memmap[DEV_PIC].base);
    qemu_fdt_add_subnode(fdt, nodename);
    qemu_fdt_setprop_string(fdt, nodename, "compatible", "lupio,pic");
    qemu_fdt_setprop(fdt, nodename, "interrupt-controller", NULL, 0);
    qemu_fdt_setprop_cell(fdt, nodename, "#interrupt-cells", 0x1);
    qemu_fdt_setprop_cells(fdt, nodename, "reg",
                           memmap[DEV_PIC].base, memmap[DEV_PIC].size);
    qemu_fdt_setprop_cell(fdt, nodename, "phandle", phandle_pic);
    g_free(nodename);

    /* TMR attached to PIC's per-cpu IRQs */
    nodename = g_strdup_printf("/soc/lupio-tmr@%" PRIx64,
                               memmap[DEV_TMR].base);
    qemu_fdt_add_subnode(fdt, nodename);
    qemu_fdt_setprop_string(fdt, nodename, "compatible", "lupio,tmr");
    qemu_fdt_setprop_cells(fdt, nodename, "reg",
                           memmap[DEV_TMR].base, memmap[DEV_TMR].size);
    qemu_fdt_setprop_cells(fdt, nodename, "clocks", phandle_clk);
    cells = g_new0(uint32_t, smp_cpus);
    for (i = 0; i < smp_cpus; i++)
        cells[i] = cpu_to_be32(i * LUPA32_PCPU_MAX + LUPA32_TMR_IRQ);
    qemu_fdt_setprop(fdt, nodename, "interrupts", cells,
                     smp_cpus * sizeof(uint32_t));
    g_free(cells);
    g_free(nodename);

    /* IPI attached to PIC's per-cpu IRQs */
    nodename = g_strdup_printf("/soc/lupio-ipi@%" PRIx64,
                               memmap[DEV_IPI].base);
    qemu_fdt_add_subnode(fdt, nodename);
    qemu_fdt_setprop_string(fdt, nodename, "compatible", "lupio,ipi");
    qemu_fdt_setprop(fdt, nodename, "interrupt-controller", NULL, 0);
    qemu_fdt_setprop_cells(fdt, nodename, "reg",
                           memmap[DEV_IPI].base, memmap[DEV_IPI].size);
    cells = g_new0(uint32_t, smp_cpus);
    for (i = 0; i < smp_cpus; i++)
        cells[i] = cpu_to_be32(i * LUPA32_PCPU_MAX + LUPA32_IPI_IRQ);
    qemu_fdt_setprop(fdt, nodename, "interrupts", cells,
                     smp_cpus * sizeof(uint32_t));
    g_free(cells);
    g_free(nodename);

    /* System controller */
    nodename = g_strdup_printf("/soc/lupio-sys@%" PRIx64, memmap[DEV_SYS].base);
    qemu_fdt_add_subnode(fdt, nodename);
    qemu_fdt_setprop_string(fdt, nodename, "compatible", "syscon");
    qemu_fdt_setprop_cells(fdt, nodename, "reg",
                           memmap[DEV_SYS].base, memmap[DEV_SYS].size);
    qemu_fdt_setprop_cell(fdt, nodename, "phandle", phandle_sys);
    g_free(nodename);

    nodename = g_strdup_printf("/soc/poweroff");
    qemu_fdt_add_subnode(fdt, nodename);
    qemu_fdt_setprop_string(fdt, nodename, "compatible", "syscon-poweroff");
    qemu_fdt_setprop_cell(fdt, nodename, "regmap", phandle_sys);
    qemu_fdt_setprop_cell(fdt, nodename, "offset", 0x0);
    qemu_fdt_setprop_cell(fdt, nodename, "value", 1);
    g_free(nodename);

    nodename = g_strdup_printf("/soc/reboot");
    qemu_fdt_add_subnode(fdt, nodename);
    qemu_fdt_setprop_string(fdt, nodename, "compatible", "syscon-reboot");
    qemu_fdt_setprop_cell(fdt, nodename, "regmap", phandle_sys);
    qemu_fdt_setprop_cell(fdt, nodename, "offset", 0x4);
    qemu_fdt_setprop_cell(fdt, nodename, "value", 1);
    g_free(nodename);

    /* TTY */
    nodename = g_strdup_printf("/soc/lupio-tty@%" PRIx64, memmap[DEV_TTY].base);
    qemu_fdt_add_subnode(fdt, nodename);
    qemu_fdt_setprop_string(fdt, nodename, "compatible", "lupio,tty");
    qemu_fdt_setprop_cells(fdt, nodename, "reg",
                           memmap[DEV_TTY].base, memmap[DEV_TTY].size);
    qemu_fdt_setprop_cell(fdt, nodename, "interrupts", LUPA32_TTY_IRQ);

    /* Complete chosen node based on TTY's path */
    qemu_fdt_setprop_string(fdt, "/chosen", "stdout-path", nodename);
    g_free(nodename);

    /* Block device */
    nodename = g_strdup_printf("/soc/lupio-blk@%" PRIx64, memmap[DEV_BLK].base);
    qemu_fdt_add_subnode(fdt, nodename);
    qemu_fdt_setprop_string(fdt, nodename, "compatible", "lupio,blk");
    qemu_fdt_setprop_cells(fdt, nodename, "reg",
                           memmap[DEV_BLK].base, memmap[DEV_BLK].size);
    qemu_fdt_setprop_cell(fdt, nodename, "interrupts", LUPA32_BLK_IRQ);
    g_free(nodename);

    /* Real time clock */
    nodename = g_strdup_printf("/soc/lupio-rtc@%" PRIx64, memmap[DEV_RTC].base);
    qemu_fdt_add_subnode(fdt, nodename);
    qemu_fdt_setprop_string(fdt, nodename, "compatible", "lupio,rtc");
    qemu_fdt_setprop_cells(fdt, nodename, "reg",
                           memmap[DEV_RTC].base, memmap[DEV_RTC].size);
    g_free(nodename);

    /* Random number generator */
    nodename = g_strdup_printf("/soc/lupio-rng@%" PRIx64, memmap[DEV_RNG].base);
    qemu_fdt_add_subnode(fdt, nodename);
    qemu_fdt_setprop_string(fdt, nodename, "compatible", "lupio,rng");
    qemu_fdt_setprop_cells(fdt, nodename, "reg",
                           memmap[DEV_RNG].base, memmap[DEV_RNG].size);
    qemu_fdt_setprop_cell(fdt, nodename, "interrupts", LUPA32_RNG_IRQ);
    g_free(nodename);

    *size = fdt_size;
    return fdt;
}

static void lupa32_init(MachineState *machine)
{
    LupA32MachineState *lms = LUPA32_MACHINE(machine);
    unsigned int smp_cpus = machine->smp.cpus;
    DeviceState *pic;
    qemu_irq *pic_irqs, *tmr_irqs, *ipi_irqs;
    int i;

    /* BIOS is not supported by this board */
    if (machine->firmware) {
        error_report("BIOS not supported for this machine");
        exit(1);
    }

    /* This board has fixed size RAM (512MiB or 1GiB) */
    if (machine->ram_size != 512 * MiB &&
        machine->ram_size != 1 * GiB) {
        error_report("This machine can only be used with 512MiB or 1GiB RAM");
        exit(1);
    }

    /* Only allow Cortex-A7 for this board */
    if (strcmp(machine->cpu_type, ARM_CPU_TYPE_NAME("cortex-a7")) != 0) {
        error_report("This board can only be used with cortex-a7 CPU");
        exit(1);
    }

    /* Memory subsystem */
    memory_region_add_subregion(get_system_memory(), memmap[MEM_DRAM].base,
                                machine->ram);

    /* IRQ subsystem */
    pic_irqs = g_malloc_n(smp_cpus, sizeof(qemu_irq));
    tmr_irqs = g_malloc_n(smp_cpus, sizeof(qemu_irq));
    ipi_irqs = g_malloc_n(smp_cpus, sizeof(qemu_irq));

    /* CPU subsystem */
    for (i = 0; i < smp_cpus; i++) {
        Object *cpuobj = object_new(machine->cpu_type);

        object_property_set_bool(cpuobj, "has_el3", false, &error_fatal);
        object_property_set_bool(cpuobj, "has_el2", false, &error_fatal);

        object_property_set_link(cpuobj, "memory", OBJECT(get_system_memory()),
                                 &error_abort);

        qdev_realize(DEVICE(cpuobj), NULL, &error_fatal);

        pic_irqs[i] = qdev_get_gpio_in(DEVICE(cpuobj), ARM_CPU_IRQ);
    }

    /* Core devices */
    pic = lupio_pic_create(memmap[DEV_PIC].base, smp_cpus, pic_irqs);

    for (i = 0; i < smp_cpus; i++) {
        tmr_irqs[i] = qdev_get_gpio_in(DEVICE(pic),
                                       i * LUPA32_PCPU_MAX + LUPA32_TMR_IRQ);
        ipi_irqs[i] = qdev_get_gpio_in(DEVICE(pic),
                                       i * LUPA32_PCPU_MAX + LUPA32_IPI_IRQ);
    }

    lupio_tmr_create(memmap[DEV_TMR].base, smp_cpus, tmr_irqs, TIMEBASE_FREQ);

    lupio_ipi_create(memmap[DEV_IPI].base, smp_cpus, ipi_irqs);

    g_free(pic_irqs);
    g_free(tmr_irqs);
    g_free(ipi_irqs);

    /* Peripheral devices */
    lupio_blk_create(memmap[DEV_BLK].base,
                     qdev_get_gpio_in(DEVICE(pic), LUPA32_BLK_IRQ),
                     drive_get(IF_NONE, 0, 0));

    lupio_rng_create(memmap[DEV_RNG].base,
                     qdev_get_gpio_in(DEVICE(pic), LUPA32_RNG_IRQ));

    lupio_rtc_create(memmap[DEV_RTC].base);

    lupio_sys_create(memmap[DEV_SYS].base);

    lupio_tty_create(memmap[DEV_TTY].base,
                     qdev_get_gpio_in(DEVICE(pic), LUPA32_TTY_IRQ),
                     serial_hd(0));

    /* Kernel and FDT loading */
    lms->binfo.loader_start = memmap[MEM_DRAM].base;
    lms->binfo.ram_size = machine->ram_size;
    lms->binfo.get_dtb = lupa32_get_dtb;
    lms->binfo.psci_conduit = QEMU_PSCI_CONDUIT_HVC;
    arm_load_kernel(ARM_CPU(first_cpu), machine, &lms->binfo);
}

static void lupa32_machine_class_init(ObjectClass *oc, void *data)
{
    MachineClass *mc = MACHINE_CLASS(oc);
    mc->desc = "LupA32 (ARM32+LupIO) board";
    mc->init = lupa32_init;
    mc->max_cpus = MAX_CPUS;
    mc->default_cpu_type = ARM_CPU_TYPE_NAME("cortex-a7");
    mc->default_ram_size = 1 * GiB;
    mc->default_ram_id = "lupa32.ram";
}

static const TypeInfo lupa32_machine_info = {
    .name          = TYPE_LUPA32_MACHINE,
    .parent        = TYPE_MACHINE,
    .instance_size = sizeof(LupA32MachineState),
    .class_init    = lupa32_machine_class_init,
};

static void lupa32_machine_register_types(void)
{
    type_register_static(&lupa32_machine_info);
}

type_init(lupa32_machine_register_types);

