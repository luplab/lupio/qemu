// SPDX-License-Identifier: GPL-2.0-or-later

/*
 * LupIO-blk
 *
 * Block virtual device
 */
#include "qemu/osdep.h"
#include "block/block.h"
#include "exec/address-spaces.h"
#include "exec/hwaddr.h"
#include "exec/memory.h"
#include "hw/irq.h"
#include "hw/lupio/blk.h"
#include "hw/qdev-core.h"
#include "hw/qdev-properties-system.h"
#include "hw/sysbus.h"
#include "qapi/error.h"
#include "qemu/error-report.h"
#include "qemu/log.h"
#include "qemu/module.h"
#include "qemu/typedefs.h"
#include "qom/object.h"
#include "sysemu/block-backend.h"
#include "sysemu/blockdev.h"

/* Register map */
enum {
    LUPIO_BLK_CONF,

    LUPIO_BLK_NBLK,
    LUPIO_BLK_BLKA,
    LUPIO_BLK_MEMA,

    LUPIO_BLK_CTRL,
    LUPIO_BLK_STAT,

    /* Max offset */
    LUPIO_BLK_MAX,
};

/* Shared fields for CTRL and STAT registers */
#define LUPIO_BLK_TYPE  0x2

/* Specific fields for CTRL */
#define LUPIO_BLK_IRQE  0x1

/* Specific fields for STAT */
#define LUPIO_BLK_BUSY  0x1
#define LUPIO_BLK_ERRR  0x4

/* Qemu Object Model */
#define TYPE_LUPIO_BLK "lupio-blk"
OBJECT_DECLARE_SIMPLE_TYPE(LupioBLKState, LUPIO_BLK)

struct LupioBLKState {
    SysBusDevice parent_obj;

    MemoryRegion mmio;
    BlockBackend *blk;
    qemu_irq irq;

    /* Properties */
    uint64_t nb_blocks; // could be up to 2^32

    /* Internal registers */
    uint32_t nblk, lba, mem;    // Transfer parameters
    bool rw, err;               // Status
};

/*
 * MMIO frontend
 */
static uint64_t lupio_blk_read(void *opaque, hwaddr addr, unsigned int size)
{
    LupioBLKState *s = LUPIO_BLK(opaque);
    uint64_t r = 0;

    if (!s->blk)
        return 0;

    switch (addr >> 2) {
        case LUPIO_BLK_CONF:
            r = BDRV_SECTOR_BITS << 16          /* log2(512B / block) */
                | (63 - clz64(s->nb_blocks));   /* log2(Nb of blocks) */
            break;
        case LUPIO_BLK_NBLK:
            r = s->nblk;
            break;
        case LUPIO_BLK_BLKA:
                r = s->lba;
            break;
        case LUPIO_BLK_MEMA:
                r = s->mem;
            break;

        case LUPIO_BLK_STAT:
            r = 0;                  /* Always idle */
            if (s->rw)
                r |= LUPIO_BLK_TYPE;    /* Write command */
            if (s->err)
                r |= LUPIO_BLK_ERRR;    /* Error */

            /* Acknowledge IRQ */
            qemu_irq_lower(s->irq);
            break;

        default:
            qemu_log_mask(LOG_GUEST_ERROR, "%s: Bad offset "TARGET_FMT_plx"\n",
                          __func__, addr);
            break;
    }

    return r;
}

static void lupio_blk_cmd(LupioBLKState *s)
{
    AddressSpace *as = &address_space_memory;

    hwaddr mem = s->mem;
    uint32_t lba = s->lba,
             nblk = s->nblk;

    size_t req_len;
    void *req_data = NULL;
    int64_t offset;

    /* Check parameters */
    if ((uint64_t)lba + nblk > s->nb_blocks) {
        qemu_log_mask(LOG_GUEST_ERROR, "%s: Bad LBA range\n", __func__);
        goto err;
    }
    if (!memory_region_present(as->root, mem)
        || !memory_region_present(as->root, mem + nblk * BDRV_SECTOR_SIZE)) {
        qemu_log_mask(LOG_GUEST_ERROR, "%s: Bad address range "TARGET_FMT_plx"\n",
                      __func__, mem);
        goto err;
    }

#if 0
    fprintf(stderr, "%s: lba=%"PRIu32", mem=0x%"PRIx64", nblk=%u, write=%d\n",
            __func__, lba, mem, nblk, s->rw);
#endif

    /* Perform transfer */
    req_len = nblk * BDRV_SECTOR_SIZE;
    req_data = malloc(req_len);
    offset = (int64_t)lba << BDRV_SECTOR_BITS;

    if (!s->rw) {
        /* Read command (block -> mem) */
        if (blk_pread(s->blk, offset, req_data, req_len) < 0) {
            error_report("%s: Can't read block from block device", __func__);
            goto err;
        }

        if (address_space_write(&address_space_memory, mem,
                                MEMTXATTRS_UNSPECIFIED, req_data, req_len)) {
            error_report("%s: Can't write block to memory", __func__);
            goto err;
        }

    } else {
        /* Write command (mem -> block) */
        if (address_space_read(&address_space_memory, mem,
                               MEMTXATTRS_UNSPECIFIED, req_data, req_len)) {
            error_report("%s: Can't read block from memory", __func__);
            goto err;
        }
        if (blk_pwrite(s->blk, offset, req_data, req_len, 0) < 0) {
            error_report("%s: Can't write block to block device", __func__);
            goto err;
        }
    }

    goto ret;

err:
    s->err = true;
ret:
    if (req_data)
        free(req_data);
    return;
}

static void lupio_blk_write(void *opaque, hwaddr addr, uint64_t val64, unsigned
                             int size)
{
    LupioBLKState *s = LUPIO_BLK(opaque);
    uint32_t val = val64;

    if (!s->blk)
        return;

    switch (addr >> 2) {
        case LUPIO_BLK_NBLK:
            s->nblk = val;
            break;
        case LUPIO_BLK_BLKA:
            s->lba = val;
            break;
        case LUPIO_BLK_MEMA:
            s->mem = val;
            break;

        case LUPIO_BLK_CTRL:
            s->err = false;
            s->rw = !!(val & LUPIO_BLK_TYPE);

            /* Perform command */
            lupio_blk_cmd(s);

            /* Raise IRQ if requested */
            if (val & LUPIO_BLK_IRQE)
                qemu_irq_raise(s->irq);
            break;

        default:
            qemu_log_mask(LOG_GUEST_ERROR, "%s: Bad offset "TARGET_FMT_plx"\n",
                          __func__, addr);
            break;
    }
}

static const MemoryRegionOps lupio_blk_ops = {
    .read       = lupio_blk_read,
    .write      = lupio_blk_write,
    .endianness = DEVICE_NATIVE_ENDIAN,
    .valid = {
        .min_access_size = 4,
        .max_access_size = 4,
    }
};

static void lupio_blk_init(Object *obj)
{
    LupioBLKState *s = LUPIO_BLK(obj);

    /* Configure MMIO region */
    memory_region_init_io(&s->mmio, obj, &lupio_blk_ops, s, TYPE_LUPIO_BLK,
                          LUPIO_BLK_MAX * sizeof(uint32_t));

    /* Attach device to bus */
    sysbus_init_irq(SYS_BUS_DEVICE(obj), &s->irq);
    sysbus_init_mmio(SYS_BUS_DEVICE(obj), &s->mmio);
}

/*
 * Class definition
 */
static Property lupio_blk_props[] = {
    DEFINE_PROP_DRIVE("drive", LupioBLKState, blk), \
    DEFINE_PROP_END_OF_LIST(),
};

static void lupio_blk_realize(DeviceState *dev, Error **errpp)
{
    LupioBLKState *s = LUPIO_BLK(dev);

    /* If an image was provided via --drive */
    if (s->blk) {

        /* Check permissions */
        if (!blk_supports_write_perm(s->blk)) {
            error_setg(errpp, "Can't use a read-only drive");
            return;
        }

        /* Set permissions */
        if (blk_set_perm(s->blk, BLK_PERM_CONSISTENT_READ | BLK_PERM_WRITE, BLK_PERM_ALL, errpp) < 0) {
            error_setg(errpp, "Can't set up permissions for drive");
            return;
        }

        /* Check image size */
        s->nb_blocks = blk_nb_sectors(s->blk);
        assert(s->nb_blocks > 0);
        if (!is_power_of_2(s->nb_blocks) || s->nb_blocks > (1ULL << 32)) {
            error_setg(errpp, "Number of 512B-blocks must be a power of 2,"
                       " and less than or equal to 2^32");
            return;
        }
    }
}

static void lupio_blk_reset(DeviceState *dev)
{
    LupioBLKState *s = LUPIO_BLK(dev);

    /* Disable interrupt */
    qemu_irq_lower(s->irq);

    /* Reset all internal registers */
    s->nblk = 0;
    s->lba = 0;
    s->mem = 0;
    s->rw = false;
    s->err = false;
}

static void lupio_blk_class_init(ObjectClass *klass, void *data)
{
    DeviceClass *dc = DEVICE_CLASS(klass);

    /* Override virtual methods */
    dc->reset = lupio_blk_reset;
    dc->realize = lupio_blk_realize;

    /* Set device properties */
    device_class_set_props(dc, lupio_blk_props);
}


/*
 * Object definition
 */

static const TypeInfo lupio_blk_info = {
    /* Basic fields */
    .name           = TYPE_LUPIO_BLK,
    .parent         = TYPE_SYS_BUS_DEVICE,
    /* Instance */
    .instance_size  = sizeof(LupioBLKState),
    .instance_init  = lupio_blk_init,
    /* Class */
    .class_init     = lupio_blk_class_init,
};

static void lupio_blk_register_types(void)
{
    type_register_static(&lupio_blk_info);
}

type_init(lupio_blk_register_types);

/*
 * Creation API (to be called from platform)
 */
DeviceState *lupio_blk_create(hwaddr addr, qemu_irq irq, DriveInfo *drv)
{
    DeviceState *dev;
    SysBusDevice *s;

    dev = qdev_new(TYPE_LUPIO_BLK);
    s = SYS_BUS_DEVICE(dev);
    if (drv)
        qdev_prop_set_drive_err(dev, "drive", blk_by_legacy_dinfo(drv), &error_fatal);
    sysbus_realize_and_unref(s, &error_fatal);
    sysbus_mmio_map(s, 0, addr);
    sysbus_connect_irq(s, 0, irq);

    return dev;
}
