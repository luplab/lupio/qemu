// SPDX-License-Identifier: GPL-2.0-or-later

/*
 * LupIO-pic
 *
 * Programmable interrupt controller virtual device
 */
#include "qemu/osdep.h"
#include "exec/hwaddr.h"
#include "exec/memory.h"
#include "hw/qdev-core.h"
#include "hw/qdev-properties.h"
#include "hw/irq.h"
#include "hw/lupio/pic.h"
#include "hw/sysbus.h"
#include "qapi/error.h"
#include "qemu/log.h"
#include "qemu/module.h"
#include "qemu/typedefs.h"
#include "qom/object.h"

/* Register map */
enum {
    LUPIO_PIC_PRIO  = 0x0,
    LUPIO_PIC_MASK  = 0x4,
    LUPIO_PIC_PEND  = 0x8,
    LUPIO_PIC_ENAB  = 0xC,

    /* Max offset */
    LUPIO_PIC_MAX   = 0x10,
};

/* Qemu Object Model */
#define TYPE_LUPIO_PIC "lupio-pic"
OBJECT_DECLARE_SIMPLE_TYPE(LupioPICState, LUPIO_PIC)

#define LUPIO_PIC_NSRC      32
#define LUPIO_PIC_NCPU      32

struct LupioPICState {
    SysBusDevice parent_obj;

    MemoryRegion mmio;
    qemu_irq parent_irq[LUPIO_PIC_NCPU];

    /* Properties */
    uint32_t ncpus;

    /* Internal registers */
    uint32_t pending;
    uint32_t mask[LUPIO_PIC_NCPU];
    uint32_t enable[LUPIO_PIC_NCPU];
};

/*
 * IRQ management
 */
static void lupio_pic_update_irq(LupioPICState *s)
{
    int cpu;

    for (cpu = 0; cpu < s->ncpus; cpu++) {
        int level = !!(s->enable[cpu] & s->pending & s->mask[cpu]);
        qemu_set_irq(s->parent_irq[cpu], level);
    }
}

static void lupio_pic_irq_request(void *opaque, int irq, int level)
{
    LupioPICState *s = LUPIO_PIC(opaque);
    uint32_t irq_mask = 1UL << irq;

    if (level)
        s->pending |= irq_mask;
    else
        s->pending &= ~irq_mask;

    lupio_pic_update_irq(s);
}

/*
 * MMIO frontend
 */
static uint64_t lupio_pic_read(void *opaque, hwaddr addr, unsigned int size)
{
    LupioPICState *s = LUPIO_PIC(opaque);
    uint32_t r = 0;
    int cpu, reg;

    assert(size == sizeof(uint32_t));

    cpu = addr / LUPIO_PIC_MAX;
    reg = addr % LUPIO_PIC_MAX;

    switch (reg) {
        case LUPIO_PIC_PRIO:
            /* Value will be 32 if no enabled and unmasked pending IRQ */
            r = ctz32(s->enable[cpu] & s->mask[cpu] & s->pending);
            break;
        case LUPIO_PIC_MASK:
            r = s->mask[cpu];
            break;
        case LUPIO_PIC_PEND:
			r = (s->enable[cpu] & s->pending);
            break;
        case LUPIO_PIC_ENAB:
			r = s->enable[cpu];
            break;

        default:
            qemu_log_mask(LOG_GUEST_ERROR, "%s: Bad offset "TARGET_FMT_plx"\n",
                          __func__, addr);
    }

    return r;
}

static void lupio_pic_write(void *opaque, hwaddr addr, uint64_t val64, unsigned
                            int size)
{
    LupioPICState *s = LUPIO_PIC(opaque);
    uint32_t val = val64;
    int cpu, reg;

    assert(size == sizeof(uint32_t));

    cpu = addr / LUPIO_PIC_MAX;
    reg = addr % LUPIO_PIC_MAX;

    switch (reg) {
        case LUPIO_PIC_MASK:
            s->mask[cpu] = val;
            lupio_pic_update_irq(s);
            break;
        case LUPIO_PIC_ENAB:
            s->enable[cpu] = val;
            lupio_pic_update_irq(s);
            break;

        default:
            qemu_log_mask(LOG_GUEST_ERROR, "%s: Bad offset "TARGET_FMT_plx"\n",
                          __func__, addr);
            break;
    }
}

static const MemoryRegionOps lupio_pic_ops = {
    .read       = lupio_pic_read,
    .write      = lupio_pic_write,
    .endianness = DEVICE_NATIVE_ENDIAN,
    .valid = {
        .min_access_size = 4,
        .max_access_size = 4,
    }
};

static void lupio_pic_init(Object *obj)
{
    LupioPICState *s = LUPIO_PIC(obj);
    DeviceState *dev = DEVICE(obj);
    int cpu;

    /* Configure MMIO region */
    memory_region_init_io(&s->mmio, obj, &lupio_pic_ops, s, TYPE_LUPIO_PIC,
                          LUPIO_PIC_NCPU * LUPIO_PIC_MAX * sizeof(uint32_t));

    /* Attach device to bus */
    for (cpu = 0; cpu < LUPIO_PIC_NCPU; cpu++)
        sysbus_init_irq(SYS_BUS_DEVICE(obj), &s->parent_irq[cpu]);
    sysbus_init_mmio(SYS_BUS_DEVICE(obj), &s->mmio);

    /* Create IRQ lines for sources */
    qdev_init_gpio_in(dev, lupio_pic_irq_request, LUPIO_PIC_NSRC);
}

/*
 * Class definition
 */
static Property lupio_pic_props[] = {
    DEFINE_PROP_UINT32("num-cpus", LupioPICState, ncpus, 1),
    DEFINE_PROP_END_OF_LIST(),
};

static void lupio_pic_realize(DeviceState *dev, Error **errpp)
{
    LupioPICState *s = LUPIO_PIC(dev);

    if (s->ncpus > LUPIO_PIC_NCPU) {
        error_setg(errpp, "Requested %u CPUs exceeds PIC maximum %d",
                   s->ncpus, LUPIO_PIC_NCPU);
        return;
    }
}

static void lupio_pic_reset(DeviceState *dev)
{
    LupioPICState *s = LUPIO_PIC(dev);

    /* Reset LupioPICState */
    s->pending = 0;
    memset(s->enable, 0, LUPIO_PIC_NCPU * sizeof(uint32_t));
    memset(s->mask, 0, LUPIO_PIC_NCPU * sizeof(uint32_t));

    /* CPU0 receives all IRQ sources by default */
    s->enable[0] = ~(uint32_t)0;

    /* Lower all interrupt */
    lupio_pic_update_irq(s);
}

static void lupio_pic_class_init(ObjectClass *klass, void *data)
{
    DeviceClass *dc = DEVICE_CLASS(klass);

    /* Override virtual methods */
    dc->reset = lupio_pic_reset;
    dc->realize = lupio_pic_realize;

    /* Set device properties */
    device_class_set_props(dc, lupio_pic_props);
}


/*
 * Object definition
 */

static const TypeInfo lupio_pic_info = {
    /* Basic fields */
    .name           = TYPE_LUPIO_PIC,
    .parent         = TYPE_SYS_BUS_DEVICE,
    /* Instance */
    .instance_size  = sizeof(LupioPICState),
    .instance_init  = lupio_pic_init,
    /* Class */
    .class_init     = lupio_pic_class_init,
};

static void lupio_pic_register_types(void)
{
    type_register_static(&lupio_pic_info);
}

type_init(lupio_pic_register_types);

/*
 * Creation API (to be called from platform)
 */
DeviceState *lupio_pic_create(hwaddr addr, uint32_t ncpus, qemu_irq *irq)
{
    DeviceState *dev;
    SysBusDevice *s;
    int cpu;

    dev = qdev_new(TYPE_LUPIO_PIC);
    s = SYS_BUS_DEVICE(dev);
    qdev_prop_set_uint32(dev, "num-cpus", ncpus);
    sysbus_realize_and_unref(s, &error_fatal);
    sysbus_mmio_map(s, 0, addr);
    for (cpu = 0; cpu < ncpus; cpu++)
        sysbus_connect_irq(s, cpu, irq[cpu]);

    return dev;
}
