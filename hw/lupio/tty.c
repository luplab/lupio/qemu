// SPDX-License-Identifier: GPL-2.0-or-later

/*
 * LupIO-tty
 *
 * Terminal virtual device
 */
#include "qemu/osdep.h"
#include "chardev/char.h"
#include "chardev/char-fe.h"
#include "exec/hwaddr.h"
#include "exec/memory.h"
#include "hw/qdev-core.h"
#include "hw/qdev-properties-system.h"
#include "hw/irq.h"
#include "hw/lupio/tty.h"
#include "hw/sysbus.h"
#include "qapi/error.h"
#include "qemu/log.h"
#include "qemu/module.h"
#include "qemu/typedefs.h"
#include "qom/object.h"

/* Register map */
enum {
    LUPIO_TTY_WRIT,
    LUPIO_TTY_READ,
    LUPIO_TTY_CTRL,
    LUPIO_TTY_STAT,

    /* Max offset */
    LUPIO_TTY_MAX,
};

#define LUPIO_TTY_INVAL    0x80000000

/* Same fields for CTRL and STAT registers */
#define LUPIO_TTY_WBIT  (1 << 0)
#define LUPIO_TTY_RBIT  (1 << 1)

/* Qemu Object Model */
#define TYPE_LUPIO_TTY "lupio-tty"
OBJECT_DECLARE_SIMPLE_TYPE(LupioTTYState, LUPIO_TTY)

struct LupioTTYState {
    SysBusDevice parent_obj;

    MemoryRegion mmio;
    CharBackend chr;
    qemu_irq irq;

    /* Internal registers */
    int8_t writ_char, read_char;
    bool writ_ie, read_ie;
};

/*
 * IRQ management
 */
static void lupio_tty_update_irq(LupioTTYState *s)
{
    unsigned int irq;

    irq = (s->writ_ie && s->writ_char != -1)
        || (s->read_ie && s->read_char != -1);
    qemu_set_irq(s->irq, irq);
}

/*
 * MMIO frontend
 */
static uint64_t lupio_tty_read(void *opaque, hwaddr addr, unsigned int size)
{
    LupioTTYState *s = LUPIO_TTY(opaque);
    uint32_t r = LUPIO_TTY_INVAL;

    assert(size == sizeof(uint32_t));

    switch (addr >> 2) {
        case LUPIO_TTY_WRIT:
            if (s->writ_char != -1) {
                /* Reading last written character lowers IRQ */
                r = s->writ_char;
                s->writ_char = -1;
                lupio_tty_update_irq(s);
            }
            break;
        case LUPIO_TTY_READ:
            if (s->read_char != -1) {
                /* Return new character */
                r = s->read_char;
                s->read_char = -1;
                lupio_tty_update_irq(s);
            }
            break;
        case LUPIO_TTY_CTRL:
			r = 0;
			if (s->writ_ie)
				r |= LUPIO_TTY_WBIT;
			if (s->read_ie)
				r |= LUPIO_TTY_RBIT;
            break;
        case LUPIO_TTY_STAT:
            /* Always ready to write */
            r = LUPIO_TTY_WBIT;
            /* Ready to read if unread character available */
            if (s->read_char != -1)
                r |= LUPIO_TTY_RBIT;
            break;

        default:
            qemu_log_mask(LOG_GUEST_ERROR, "%s: Bad offset "TARGET_FMT_plx"\n",
                          __func__, addr);
    }

    return r;
}

static void lupio_tty_write(void *opaque, hwaddr addr, uint64_t val64, unsigned
                             int size)
{
    LupioTTYState *s = LUPIO_TTY(opaque);
    uint32_t val = val64;
    uint8_t c = val;

    assert(size == sizeof(uint32_t));

    switch (addr >> 2) {
        case LUPIO_TTY_WRIT:
            qemu_chr_fe_write_all(&s->chr, &c, 1);
			s->writ_char = c;
            lupio_tty_update_irq(s);
            break;
        case LUPIO_TTY_CTRL:
			s->writ_ie = !!(val & LUPIO_TTY_WBIT);
			s->read_ie = !!(val & LUPIO_TTY_RBIT);
            lupio_tty_update_irq(s);
            break;

        default:
            qemu_log_mask(LOG_GUEST_ERROR, "%s: Bad offset "TARGET_FMT_plx"\n",
                          __func__, addr);
            break;
    }
}

static const MemoryRegionOps lupio_tty_ops = {
    .read       = lupio_tty_read,
    .write      = lupio_tty_write,
    .endianness = DEVICE_NATIVE_ENDIAN,
    .valid = {
        .min_access_size = 4,
        .max_access_size = 4,
    }
};

static void lupio_tty_init(Object *obj)
{
    LupioTTYState *s = LUPIO_TTY(obj);

    /* Configure MMIO region */
    memory_region_init_io(&s->mmio, obj, &lupio_tty_ops, s, TYPE_LUPIO_TTY,
                          LUPIO_TTY_MAX * sizeof(uint32_t));

    /* Attach device to bus */
    sysbus_init_irq(SYS_BUS_DEVICE(obj), &s->irq);
    sysbus_init_mmio(SYS_BUS_DEVICE(obj), &s->mmio);
}

/*
 * Char backend
 */
static int lupio_tty_can_receive(void *opaque)
{
    LupioTTYState *s = LUPIO_TTY(opaque);

    /* Prevent from overwritting an unread character */
    return (s->read_char == -1);
}

static void lupio_tty_receive(void *opaque, const uint8_t *buf, int size)
{
    LupioTTYState *s = LUPIO_TTY(opaque);

    /* Drop characters */
    if (s->read_char != -1) {
        fprintf(stderr, "Dropping characters\n");
        return;
    }

    s->read_char = *buf;

    lupio_tty_update_irq(s);
}

/*
 * Class definition
 */
static Property lupio_tty_props[] = {
    /* Connect char property "chardev" to LupioTTYState.chr */
    DEFINE_PROP_CHR("chardev", LupioTTYState, chr),
    DEFINE_PROP_END_OF_LIST(),
};

static void lupio_tty_realize(DeviceState *dev, Error **errpp)
{
    LupioTTYState *s = LUPIO_TTY(dev);

    /* Connect to QEMU char backend */
    qemu_chr_fe_set_handlers(&s->chr,
                             lupio_tty_can_receive, lupio_tty_receive,
                             NULL, NULL, s, NULL, true);
}

static void lupio_tty_reset(DeviceState *dev)
{
    LupioTTYState *s = LUPIO_TTY(dev);

    /* Reset LupioTTYState */
    s->writ_char = s->read_char = -1;
    s->writ_ie = s->read_ie = false;

    /* Disable interrupt */
    qemu_irq_lower(s->irq);
}

static void lupio_tty_class_init(ObjectClass *klass, void *data)
{
    DeviceClass *dc = DEVICE_CLASS(klass);

    /* Override virtual methods */
    dc->reset = lupio_tty_reset;
    dc->realize = lupio_tty_realize;

    /* Set device properties */
    device_class_set_props(dc, lupio_tty_props);
}


/*
 * Object definition
 */

static const TypeInfo lupio_tty_info = {
    /* Basic fields */
    .name           = TYPE_LUPIO_TTY,
    .parent         = TYPE_SYS_BUS_DEVICE,
    /* Instance */
    .instance_size  = sizeof(LupioTTYState),
    .instance_init  = lupio_tty_init,
    /* Class */
    .class_init     = lupio_tty_class_init,
};

static void lupio_tty_register_types(void)
{
    type_register_static(&lupio_tty_info);
}

type_init(lupio_tty_register_types);

/*
 * Creation API (to be called from platform)
 */
DeviceState *lupio_tty_create(hwaddr addr, qemu_irq irq, Chardev *chr)
{
    DeviceState *dev;
    SysBusDevice *s;

    dev = qdev_new(TYPE_LUPIO_TTY);
    s = SYS_BUS_DEVICE(dev);
    qdev_prop_set_chr(dev, "chardev", chr);
    sysbus_realize_and_unref(s, &error_fatal);
    sysbus_mmio_map(s, 0, addr);
    sysbus_connect_irq(s, 0, irq);

    return dev;
}
