// SPDX-License-Identifier: GPL-2.0-or-later

/*
 * LupIO-sys
 *
 * System controller virtual device
 */
#include "qemu/osdep.h"

#include "exec/hwaddr.h"
#include "hw/lupio/sys.h"
#include "hw/sysbus.h"
#include "qapi/error.h"
#include "qemu/log.h"
#include "qemu/module.h"
#include "sysemu/sysemu.h"
#include "sysemu/runstate.h"

/* Register map */
enum {
    LUPIO_SYS_HALT,
    LUPIO_SYS_REBT,

    /* Max offset */
    LUPIO_SYS_MAX,
};

/* Qemu Object Model */
#define TYPE_LUPIO_SYS "lupio-sys"
OBJECT_DECLARE_SIMPLE_TYPE(LupioSYSState, LUPIO_SYS)

struct LupioSYSState {
    SysBusDevice parent_obj;

    MemoryRegion mmio;
};

/*
 * MMIO frontend
 */
static uint64_t lupio_sys_read(void *opaque, hwaddr addr, unsigned int size)
{
    assert(size == sizeof(uint32_t));
    qemu_log_mask(LOG_GUEST_ERROR, "%s: Bad offset "TARGET_FMT_plx"\n",
                  __func__, addr);

    return 0;
}

static void lupio_sys_write(void *opaque, hwaddr addr, uint64_t val64,
                            unsigned int size)
{
    assert(size == sizeof(uint32_t));

    switch (addr >> 2) {
        case LUPIO_SYS_HALT:
            qemu_system_shutdown_request(SHUTDOWN_CAUSE_GUEST_SHUTDOWN);
            break;
        case LUPIO_SYS_REBT:
            qemu_system_reset_request(SHUTDOWN_CAUSE_GUEST_RESET);
            break;

        default:
            qemu_log_mask(LOG_GUEST_ERROR, "%s: Bad offset "TARGET_FMT_plx"\n",
                          __func__, addr);
            break;
    }
}

static const MemoryRegionOps lupio_sys_ops = {
    .read       = lupio_sys_read,
    .write      = lupio_sys_write,
    .endianness = DEVICE_NATIVE_ENDIAN,
    .valid = {
        .min_access_size = 4,
        .max_access_size = 4,
    }
};

static void lupio_sys_init(Object *obj)
{
    LupioSYSState *s = LUPIO_SYS(obj);

    /* Configure MMIO region */
    memory_region_init_io(&s->mmio, obj, &lupio_sys_ops, s, TYPE_LUPIO_SYS,
                          LUPIO_SYS_MAX * sizeof(uint32_t));

    /* Attach device to bus */
    sysbus_init_mmio(SYS_BUS_DEVICE(obj), &s->mmio);
}

/*
 * Object definition
 */
static const TypeInfo lupio_sys_info = {
    /* Basic fields */
    .name           = TYPE_LUPIO_SYS,
    .parent         = TYPE_SYS_BUS_DEVICE,
    /* Instance */
    .instance_size  = sizeof(LupioSYSState),
    .instance_init  = lupio_sys_init,
};

static void lupio_sys_register_types(void)
{
    type_register_static(&lupio_sys_info);
}

type_init(lupio_sys_register_types);

/*
 * Creation API (to be called from platform)
 */
DeviceState *lupio_sys_create(hwaddr addr)
{
    DeviceState *dev;
    SysBusDevice *s;

    dev = qdev_new(TYPE_LUPIO_SYS);
    s = SYS_BUS_DEVICE(dev);
    sysbus_realize_and_unref(s, &error_fatal);
    sysbus_mmio_map(s, 0, addr);

    return dev;
}

