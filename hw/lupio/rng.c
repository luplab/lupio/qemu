// SPDX-License-Identifier: GPL-2.0-or-later

/*
 * LupIO-rng
 *
 * Random number generator virtual device
 */
#include "qemu/osdep.h"

#include "exec/hwaddr.h"
#include "hw/irq.h"
#include "hw/lupio/rng.h"
#include "hw/qdev-core.h"
#include "hw/qdev-properties.h"
#include "hw/sysbus.h"
#include "qapi/error.h"
#include "qemu/guest-random.h"
#include "qemu/log.h"
#include "qemu/module.h"
#include "sysemu/sysemu.h"

/* Register map */
enum {
    LUPIO_RNG_RAND,
    LUPIO_RNG_SEED,
    LUPIO_RNG_CTRL,
    LUPIO_RNG_STAT,

    /* Max offset */
    LUPIO_RNG_MAX,
};

/* CTRL fields */
#define LUPIO_RNG_IRQE  0x1
/* STAT fields */
#define LUPIO_RNG_BUSY  0x1

/* Qemu Object Model */
#define TYPE_LUPIO_RNG "lupio-rng"
OBJECT_DECLARE_SIMPLE_TYPE(LupioRNGState, LUPIO_RNG)

struct LupioRNGState {
    SysBusDevice parent_obj;

    MemoryRegion mmio;
    qemu_irq irq;

    /* Internal registers */
    uint32_t seed;
    bool ie;
};

/*
 * MMIO frontend
 */
static uint64_t lupio_rng_read(void *opaque, hwaddr addr, unsigned int size)
{
    LupioRNGState *s = LUPIO_RNG(opaque);
    uint32_t r = 0;

    assert(size == sizeof(uint32_t));

    switch (addr >> 2) {
        case LUPIO_RNG_RAND:
            qemu_guest_getrandom_nofail(&r, sizeof(r));
            break;
        case LUPIO_RNG_SEED:
            r = s->seed;
            break;

        case LUPIO_RNG_STAT:
            r = 0;  /* Always ready */
            qemu_irq_lower(s->irq);
            break;

        default:
            qemu_log_mask(LOG_GUEST_ERROR, "%s: Bad offset "TARGET_FMT_plx"\n",
                          __func__, addr);
            break;
    }

    return r;
}

static void lupio_rng_write(void *opaque, hwaddr addr, uint64_t val64,
                            unsigned int size)
{
    LupioRNGState *s = LUPIO_RNG(opaque);

    assert(size == sizeof(uint32_t));

    switch (addr >> 2) {
        case LUPIO_RNG_SEED:
            s->seed = val64;
            qemu_guest_random_seed_thread_part2(s->seed);
            break;

        case LUPIO_RNG_CTRL:
            s->ie = !!(val64 & LUPIO_RNG_IRQE);

            if (s->ie)
                /* Our RNG model doesn't have any latency, so raise irq immediately */
                qemu_irq_raise(s->irq);
            break;

        default:
            qemu_log_mask(LOG_GUEST_ERROR, "%s: Bad offset "TARGET_FMT_plx"\n",
                          __func__, addr);
            break;
    }
}

static const MemoryRegionOps lupio_rng_ops = {
    .read       = lupio_rng_read,
    .write      = lupio_rng_write,
    .endianness = DEVICE_NATIVE_ENDIAN,
    .valid = {
        .min_access_size = 4,
        .max_access_size = 4,
    }
};

static void lupio_rng_init(Object *obj)
{
    LupioRNGState *s = LUPIO_RNG(obj);

    /* Configure MMIO region */
    memory_region_init_io(&s->mmio, obj, &lupio_rng_ops, s, TYPE_LUPIO_RNG,
                          LUPIO_RNG_MAX * sizeof(uint32_t));

    /* Attach device to bus */
    sysbus_init_irq(SYS_BUS_DEVICE(obj), &s->irq);
    sysbus_init_mmio(SYS_BUS_DEVICE(obj), &s->mmio);
}

/*
 * Class definition
 */
static void lupio_rng_reset(DeviceState *dev)
{
    LupioRNGState *s = LUPIO_RNG(dev);

    /* Disable interrupt */
    qemu_irq_lower(s->irq);

    /* Reset all internal registers */
    s->seed = 0;
    s->ie = false;
}

static void lupio_rng_class_init(ObjectClass *klass, void *data)
{
    DeviceClass *dc = DEVICE_CLASS(klass);

    /* Override virtual methods */
    dc->reset = lupio_rng_reset;
}


/*
 * Object definition
 */
static const TypeInfo lupio_rng_info = {
    /* Basic fields */
    .name           = TYPE_LUPIO_RNG,
    .parent         = TYPE_SYS_BUS_DEVICE,
    /* Instance */
    .instance_size  = sizeof(LupioRNGState),
    .instance_init  = lupio_rng_init,
    /* Class */
    .class_init     = lupio_rng_class_init,
};

static void lupio_rng_register_types(void)
{
    type_register_static(&lupio_rng_info);
}

type_init(lupio_rng_register_types);

/*
 * Creation API (to be called from platform)
 */
DeviceState *lupio_rng_create(hwaddr addr, qemu_irq irq)
{
    DeviceState *dev;
    SysBusDevice *s;

    dev = qdev_new(TYPE_LUPIO_RNG);
    s = SYS_BUS_DEVICE(dev);
    sysbus_realize_and_unref(s, &error_fatal);
    sysbus_mmio_map(s, 0, addr);
    sysbus_connect_irq(s, 0, irq);

    return dev;
}

