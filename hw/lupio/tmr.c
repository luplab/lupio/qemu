// SPDX-License-Identifier: GPL-2.0-or-later

/*
 * LupIO-tmr
 *
 * Timer virtual device
 */
#include "qemu/osdep.h"

#include "exec/hwaddr.h"
#include "hw/irq.h"
#include "hw/lupio/tmr.h"
#include "hw/qdev-core.h"
#include "hw/qdev-properties.h"
#include "hw/sysbus.h"
#include "qapi/error.h"
#include "qemu/log.h"
#include "qemu/module.h"
#include "qemu/timer.h"
#include "sysemu/sysemu.h"

#define nstoclock(ns, freq) \
    muldiv64((ns), freq, NANOSECONDS_PER_SECOND);
#define clocktons(cl, freq) \
    muldiv64((cl), NANOSECONDS_PER_SECOND, freq);

/* Register map */
enum {
    LUPIO_TMR_TIME  = 0x0,
    LUPIO_TMR_LOAD  = 0x4,
    LUPIO_TMR_CTRL  = 0x8,
    LUPIO_TMR_STAT  = 0xC,

    /* Max offset */
    LUPIO_TMR_MAX   = 0x10,
};

/* Specific fields for CTRL */
#define LUPIO_TMR_IRQE  0x1
#define LUPIO_TMR_PRDC  0x2

/* Specific fields for STAT */
#define LUPIO_TMR_EXPD  0x1

/* Qemu Object Model */
#define TYPE_LUPIO_TMR "lupio-tmr"
OBJECT_DECLARE_SIMPLE_TYPE(LupioTMRState, LUPIO_TMR)

#define LUPIO_TMR_NCPU      32

typedef struct {
    qemu_irq irq;
    QEMUTimer timer;

    /* Properties */
    uint8_t id;

    /* Internal registers */
    uint32_t reload;
    bool ie, pd;
    bool expired;
} LupioTimer;

struct LupioTMRState {
    SysBusDevice parent_obj;

    MemoryRegion mmio;

    /* Properties */
    uint32_t freq;
    uint32_t ncpus;

    /* Internal timers */
    LupioTimer timers[LUPIO_TMR_NCPU];
};

/*
 * Find LupioTMRState from any of the individual LupioTimers
 */
static inline LupioTMRState *lupio_timer_to_state(LupioTimer *t)
{
    /* Locate the array of timers `t` belongs to */
    const LupioTimer (*timers)[] = (void *)t - (t->id * sizeof(*t));
    /* Locate the LupioTMRState instance, `timers` belongs to */
    return container_of(timers, LupioTMRState, timers);
}

/*
 * Time management
 */
static uint32_t lupio_tmr_current_time(LupioTMRState *s)
{
    return nstoclock(qemu_clock_get_ns(QEMU_CLOCK_VIRTUAL), s->freq);
}

static void lupio_tmr_set(LupioTimer *t, uint32_t freq)
{
    uint64_t next = qemu_clock_get_ns(QEMU_CLOCK_VIRTUAL) +
        clocktons(t->reload, freq);
    timer_mod(&t->timer, next);
}

static void lupio_tmr_callback(void *opaque)
{
    LupioTimer *t = opaque;
    LupioTMRState *s = lupio_timer_to_state(t);

    /* Signal expiration */
    t->expired = true;
    if (t->ie)
        qemu_irq_raise(t->irq);

    /* If periodic timer, reload */
    if (t->pd && t->reload)
        lupio_tmr_set(t, s->freq);
}

/*
 * MMIO frontend
 */
static uint64_t lupio_tmr_read(void *opaque, hwaddr addr, unsigned int size)
{
    LupioTMRState *s = LUPIO_TMR(opaque);
    LupioTimer *t;
    uint32_t r = 0;
    int cpu, reg;

    assert(size == sizeof(uint32_t));

    cpu = addr / LUPIO_TMR_MAX;
    reg = addr % LUPIO_TMR_MAX;

    t = &s->timers[cpu];

    switch (reg) {
        case LUPIO_TMR_TIME:
            r = lupio_tmr_current_time(s);
            break;
        case LUPIO_TMR_LOAD:
            r = t->reload;
            break;
        case LUPIO_TMR_STAT:
            if (t->expired)
                r |= LUPIO_TMR_EXPD;

            /* Acknowledge expiration */
            t->expired = false;
            qemu_irq_lower(t->irq);
            break;

        default:
            qemu_log_mask(LOG_GUEST_ERROR, "%s: Bad offset "TARGET_FMT_plx"\n",
                          __func__, addr);
    }

    return r;
}

static void lupio_tmr_write(void *opaque, hwaddr addr, uint64_t val64,
                              unsigned int size)
{
    LupioTMRState *s = LUPIO_TMR(opaque);
    LupioTimer *t;
    uint32_t val = val64;
    int cpu, reg;

    assert(size == sizeof(uint32_t));

    cpu = addr / LUPIO_TMR_MAX;
    reg = addr % LUPIO_TMR_MAX;

    t = &s->timers[cpu];

    switch (reg) {
        case LUPIO_TMR_LOAD:
            t->reload = val;
            break;

        case LUPIO_TMR_CTRL:
			t->ie = !!(val & LUPIO_TMR_IRQE);
			t->pd = !!(val & LUPIO_TMR_PRDC);

            /* Stop current timer if any */
            if (timer_pending(&t->timer))
                timer_del(&t->timer);

            /* If reload isn't 0, start a new one */
            if (t->reload)
                lupio_tmr_set(t, s->freq);
            break;

        default:
            qemu_log_mask(LOG_GUEST_ERROR, "%s: Bad offset "TARGET_FMT_plx"\n",
                          __func__, addr);
    }
}

static const MemoryRegionOps lupio_tmr_ops = {
    .read       = lupio_tmr_read,
    .write      = lupio_tmr_write,
    .endianness = DEVICE_NATIVE_ENDIAN,
    .valid = {
        .min_access_size = 4,
        .max_access_size = 4,
    }
};

static void lupio_tmr_init(Object *obj)
{
    LupioTMRState *s = LUPIO_TMR(obj);
    int cpu;

    /* Configure MMIO region */
    memory_region_init_io(&s->mmio, obj, &lupio_tmr_ops, s, TYPE_LUPIO_TMR,
                          LUPIO_TMR_NCPU * LUPIO_TMR_MAX * sizeof(uint32_t));

    /* Attach device to bus */
    for (cpu = 0; cpu < LUPIO_TMR_NCPU; cpu++)
        sysbus_init_irq(SYS_BUS_DEVICE(obj), &s->timers[cpu].irq);
    sysbus_init_mmio(SYS_BUS_DEVICE(obj), &s->mmio);
}

/*
 * Class definition
 */
static Property lupio_tmr_props[] = {
    DEFINE_PROP_UINT32("frequency", LupioTMRState, freq, 0),
    DEFINE_PROP_UINT32("num-cpus", LupioTMRState, ncpus, 1),
    DEFINE_PROP_END_OF_LIST(),
};

static void lupio_tmr_realize(DeviceState *dev, Error **errpp)
{
    LupioTMRState *s = LUPIO_TMR(dev);
    int cpu;

    if (s->ncpus > LUPIO_TMR_NCPU) {
        error_setg(errpp, "Requested %u CPUs exceeds TMR maximum %d",
                   s->ncpus, LUPIO_TMR_NCPU);
        return;
    }

    for (cpu = 0; cpu < s->ncpus; cpu++) {
        LupioTimer *t = &s->timers[cpu];

        t->id = cpu;
        timer_init_ns(&t->timer, QEMU_CLOCK_VIRTUAL, lupio_tmr_callback, t);
    }
}

static void lupio_tmr_reset(DeviceState *dev)
{
    LupioTMRState *s = LUPIO_TMR(dev);
    int cpu;

    /* Reset all timers */
    for (cpu = 0; cpu < s->ncpus; cpu++) {
        LupioTimer *t = &s->timers[cpu];

        /* Reset internal registers */
        t->reload = 0;
        t->ie = t->pd = t->expired = false;

        /* Disable interrupt */
        qemu_irq_lower(t->irq);
    }
}

static void lupio_tmr_class_init(ObjectClass *klass, void *data)
{
    DeviceClass *dc = DEVICE_CLASS(klass);

    /* Override virtual methods */
    dc->reset = lupio_tmr_reset;
    dc->realize = lupio_tmr_realize;

    /* Set device properties */
    device_class_set_props(dc, lupio_tmr_props);
}


/*
 * Object definition
 */
static const TypeInfo lupio_tmr_info = {
    /* Basic fields */
    .name           = TYPE_LUPIO_TMR,
    .parent         = TYPE_SYS_BUS_DEVICE,
    /* Instance */
    .instance_size  = sizeof(LupioTMRState),
    .instance_init  = lupio_tmr_init,
    /* Class */
    .class_init     = lupio_tmr_class_init,
};

static void lupio_tmr_register_types(void)
{
    type_register_static(&lupio_tmr_info);
}

type_init(lupio_tmr_register_types);

/*
 * Creation API (to be called from platform)
 */
DeviceState *lupio_tmr_create(hwaddr addr, uint32_t ncpus, qemu_irq *irq,
                              uint32_t freq)
{
    DeviceState *dev;
    SysBusDevice *s;
    int cpu;

    dev = qdev_new(TYPE_LUPIO_TMR);
    s = SYS_BUS_DEVICE(dev);
    qdev_prop_set_uint32(dev, "frequency", freq);
    qdev_prop_set_uint32(dev, "num-cpus", ncpus);
    sysbus_realize_and_unref(s, &error_fatal);
    sysbus_mmio_map(s, 0, addr);
    for (cpu = 0; cpu < ncpus; cpu++)
        sysbus_connect_irq(s, cpu, irq[cpu]);

    return dev;
}

