// SPDX-License-Identifier: GPL-2.0-or-later

/*
 * LupIO-ipi
 *
 * Inter-processor interrupt virtual device
 */
#include "qemu/osdep.h"
#include "exec/hwaddr.h"
#include "hw/irq.h"
#include "hw/lupio/ipi.h"
#include "hw/qdev-core.h"
#include "hw/qdev-properties.h"
#include "hw/sysbus.h"
#include "qapi/error.h"
#include "qemu/log.h"
#include "qemu/module.h"
#include "sysemu/sysemu.h"

/* Register map */
enum {
    LUPIO_IPI_MASK  = 0x0,
    LUPIO_IPI_PEND  = 0x4,

    /* Max offset */
    LUPIO_IPI_MAX   = 0x8,
};

/* Qemu Object Model */
#define TYPE_LUPIO_IPI "lupio-ipi"
OBJECT_DECLARE_SIMPLE_TYPE(LupioIPIState, LUPIO_IPI)

#define LUPIO_IPI_NCPU      32

struct LupioIPIState {
    SysBusDevice parent_obj;

    MemoryRegion mmio;
    qemu_irq irq[LUPIO_IPI_NCPU];

    /* Properties */
    uint32_t ncpus;

    /* Internal registers */
    uint32_t mask[LUPIO_IPI_NCPU];
    uint32_t pend[LUPIO_IPI_NCPU];
};

/*
 * IRQ management
 */
static void lupio_ipi_update_irq(LupioIPIState *s)
{
    int cpu;

    for (cpu = 0; cpu < s->ncpus; cpu++) {
        int level = !!(s->pend[cpu] & s->mask[cpu]);

        qemu_set_irq(s->irq[cpu], level);
    }
}

/*
 * MMIO frontend
 */
static uint64_t lupio_ipi_read(void *opaque, hwaddr addr, unsigned int size)
{
    LupioIPIState *s = LUPIO_IPI(opaque);
    uint32_t r = 0;
    int cpu, reg;

    assert(size == sizeof(uint32_t));

    cpu = addr / LUPIO_IPI_MAX;
    reg = addr % LUPIO_IPI_MAX;

    switch (reg) {
        case LUPIO_IPI_MASK:
            r = s->mask[cpu];
            break;
        case LUPIO_IPI_PEND:
            r = s->pend[cpu];
            break;

        default:
            qemu_log_mask(LOG_GUEST_ERROR, "%s: Bad offset "TARGET_FMT_plx"\n",
                          __func__, addr);
    }

    return r;
}

static void lupio_ipi_write(void *opaque, hwaddr addr, uint64_t val64,
                              unsigned int size)
{
    LupioIPIState *s = LUPIO_IPI(opaque);
    uint32_t val = val64;
    int cpu, reg;

    assert(size == sizeof(uint32_t));

    cpu = addr / LUPIO_IPI_MAX;
    reg = addr % LUPIO_IPI_MAX;

    switch (reg) {
        case LUPIO_IPI_MASK:
            s->mask[cpu] = val;
            break;
        case LUPIO_IPI_PEND:
            s->pend[cpu] = val;
            break;

        default:
            qemu_log_mask(LOG_GUEST_ERROR, "%s: Bad offset "TARGET_FMT_plx"\n",
                          __func__, addr);
    }

    lupio_ipi_update_irq(s);
}

static const MemoryRegionOps lupio_ipi_ops = {
    .read       = lupio_ipi_read,
    .write      = lupio_ipi_write,
    .endianness = DEVICE_NATIVE_ENDIAN,
    .valid = {
        .min_access_size = 4,
        .max_access_size = 4,
    }
};

static void lupio_ipi_init(Object *obj)
{
    LupioIPIState *s = LUPIO_IPI(obj);
    int cpu;

    /* Configure MMIO region */
    memory_region_init_io(&s->mmio, obj, &lupio_ipi_ops, s, TYPE_LUPIO_IPI,
                          LUPIO_IPI_NCPU * LUPIO_IPI_MAX * sizeof(uint32_t));

    /* Attach device to bus */
    for (cpu = 0; cpu < LUPIO_IPI_NCPU; cpu++)
        sysbus_init_irq(SYS_BUS_DEVICE(obj), &s->irq[cpu]);
    sysbus_init_mmio(SYS_BUS_DEVICE(obj), &s->mmio);
}

/*
 * Class definition
 */
static Property lupio_ipi_props[] = {
    DEFINE_PROP_UINT32("num-cpus", LupioIPIState, ncpus, 1),
    DEFINE_PROP_END_OF_LIST(),
};

static void lupio_ipi_realize(DeviceState *dev, Error **errpp)
{
    LupioIPIState *s = LUPIO_IPI(dev);

    if (s->ncpus > LUPIO_IPI_NCPU) {
        error_setg(errpp, "Requested %u CPUs exceeds IPI maximum %d",
                   s->ncpus, LUPIO_IPI_NCPU);
        return;
    }
}

static void lupio_ipi_reset(DeviceState *dev)
{
    LupioIPIState *s = LUPIO_IPI(dev);

    /* Reset LupioIPIState */
    memset(s->pend, 0, LUPIO_IPI_NCPU * sizeof(uint32_t));
    memset(s->mask, 0, LUPIO_IPI_NCPU * sizeof(uint32_t));

    /* Lower all interrupt */
    lupio_ipi_update_irq(s);
}

static void lupio_ipi_class_init(ObjectClass *klass, void *data)
{
    DeviceClass *dc = DEVICE_CLASS(klass);

    /* Override virtual methods */
    dc->reset = lupio_ipi_reset;
    dc->realize = lupio_ipi_realize;

    /* Set device properties */
    device_class_set_props(dc, lupio_ipi_props);
}


/*
 * Object definition
 */
static const TypeInfo lupio_ipi_info = {
    /* Basic fields */
    .name           = TYPE_LUPIO_IPI,
    .parent         = TYPE_SYS_BUS_DEVICE,
    /* Instance */
    .instance_size  = sizeof(LupioIPIState),
    .instance_init  = lupio_ipi_init,
    /* Class */
    .class_init     = lupio_ipi_class_init,
};

static void lupio_ipi_register_types(void)
{
    type_register_static(&lupio_ipi_info);
}

type_init(lupio_ipi_register_types);

/*
 * Creation API (to be called from platform)
 */
DeviceState *lupio_ipi_create(hwaddr addr, uint32_t ncpus, qemu_irq *irq)
{
    DeviceState *dev;
    SysBusDevice *s;
    int cpu;

    dev = qdev_new(TYPE_LUPIO_IPI);
    s = SYS_BUS_DEVICE(dev);
    qdev_prop_set_uint32(dev, "num-cpus", ncpus);
    sysbus_realize_and_unref(s, &error_fatal);
    sysbus_mmio_map(s, 0, addr);
    for (cpu = 0; cpu < ncpus; cpu++)
        sysbus_connect_irq(s, cpu, irq[cpu]);

    return dev;
}

