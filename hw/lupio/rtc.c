// SPDX-License-Identifier: GPL-2.0-or-later

/*
 * LupIO-rtc
 *
 * Real-time clock virtual device (format is ISO 8601)
 */
#include "qemu/osdep.h"

#include "exec/hwaddr.h"
#include "hw/lupio/rtc.h"
#include "hw/sysbus.h"
#include "qapi/error.h"
#include "qemu/log.h"
#include "qemu/module.h"
#include "qemu/timer.h"
#include "sysemu/sysemu.h"

/* Register map */
enum {
    LUPIO_RTC_SECD,
    LUPIO_RTC_MINT,
    LUPIO_RTC_HOUR,
    LUPIO_RTC_DYMO,
    LUPIO_RTC_MNTH,
    LUPIO_RTC_YEAR,
    LUPIO_RTC_CENT,
    LUPIO_RTC_DYWK,
    LUPIO_RTC_DYYR,

    /* Max offset */
    LUPIO_RTC_MAX,
};

/* Qemu Object Model */
#define TYPE_LUPIO_RTC "lupio-rtc"
OBJECT_DECLARE_SIMPLE_TYPE(LupioRTCState, LUPIO_RTC)

struct LupioRTCState {
    SysBusDevice parent_obj;

    MemoryRegion mmio;
};

/*
 * MMIO frontend
 */
static uint64_t lupio_rtc_read(void *opaque, hwaddr addr, unsigned int size)
{
    uint32_t r = 0;
    uint64_t time_nsec;
    time_t time_sec;
    struct tm time_bd;

    /* Get real time in seconds */
    time_nsec = qemu_clock_get_ns(rtc_clock);
    time_sec = time_nsec / NANOSECONDS_PER_SECOND;

    /* Transform into broken-down time representation */
    gmtime_r(&time_sec, &time_bd);

    assert(size == sizeof(uint8_t));

    switch (addr) {
        case LUPIO_RTC_SECD:
            r = time_bd.tm_sec;                 /* 0-60 (for leap seconds) */
            break;
        case LUPIO_RTC_MINT:
            r = time_bd.tm_min;                 /* 0-59 */
            break;
        case LUPIO_RTC_HOUR:
            r = time_bd.tm_hour;                /* 0-23 */
            break;
        case LUPIO_RTC_DYMO:
            r = time_bd.tm_mday;                /* 1-31 */
            break;
        case LUPIO_RTC_MNTH:
            r = time_bd.tm_mon + 1;             /* 1-12 */
            break;
        case LUPIO_RTC_YEAR:
            r = (time_bd.tm_year + 1900) % 100; /* 0-99 */
            break;
        case LUPIO_RTC_CENT:
            r = (time_bd.tm_year + 1900) / 100; /* 0-99 */
            break;
        case LUPIO_RTC_DYWK:
            r = 1 + (time_bd.tm_wday + 6) % 7;  /* 1-7 (Monday is 1) */
            break;
        case LUPIO_RTC_DYYR:
            r = time_bd.tm_yday + 1;            /* 1-366 (for leap years) */
            break;

        default:
            qemu_log_mask(LOG_GUEST_ERROR, "%s: Bad offset "TARGET_FMT_plx"\n",
                          __func__, addr);
            break;
    }

    return r;
}

static void lupio_rtc_write(void *opaque, hwaddr addr, uint64_t val64,
                            unsigned int size)
{
    assert(size == sizeof(uint8_t));
    qemu_log_mask(LOG_GUEST_ERROR, "%s: Bad offset "TARGET_FMT_plx"\n",
                  __func__, addr);
}

static const MemoryRegionOps lupio_rtc_ops = {
    .read       = lupio_rtc_read,
    .write      = lupio_rtc_write,
    .endianness = DEVICE_NATIVE_ENDIAN,
    .valid = {
        .min_access_size = 1,
        .max_access_size = 1,
    }
};

static void lupio_rtc_init(Object *obj)
{
    LupioRTCState *s = LUPIO_RTC(obj);

    /* Configure MMIO region */
    memory_region_init_io(&s->mmio, obj, &lupio_rtc_ops, s, TYPE_LUPIO_RTC,
                          LUPIO_RTC_MAX * sizeof(uint8_t));

    /* Attach device to bus */
    sysbus_init_mmio(SYS_BUS_DEVICE(obj), &s->mmio);
}

/*
 * Object definition
 */
static const TypeInfo lupio_rtc_info = {
    /* Basic fields */
    .name           = TYPE_LUPIO_RTC,
    .parent         = TYPE_SYS_BUS_DEVICE,
    /* Instance */
    .instance_size  = sizeof(LupioRTCState),
    .instance_init  = lupio_rtc_init,
};

static void lupio_rtc_register_types(void)
{
    type_register_static(&lupio_rtc_info);
}

type_init(lupio_rtc_register_types);

/*
 * Creation API (to be called from platform)
 */
DeviceState *lupio_rtc_create(hwaddr addr)
{
    DeviceState *dev;
    SysBusDevice *s;

    dev = qdev_new(TYPE_LUPIO_RTC);
    s = SYS_BUS_DEVICE(dev);
    sysbus_realize_and_unref(s, &error_fatal);
    sysbus_mmio_map(s, 0, addr);

    return dev;
}

