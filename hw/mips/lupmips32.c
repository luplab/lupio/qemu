// SPDX-License-Identifier: GPL-2.0-or-later

/*
 * QEMU LupMIPS32 (MIPS32+LupIO) board
 *
 * Copyright (C) 2022-2023 Joël Porquet-Lupine
 */
#include "qemu/osdep.h"
#include "qapi/error.h"

#include "elf.h"
#include "exec/address-spaces.h"
#include "hw/boards.h"
#include "hw/loader.h"
#include "hw/lupio/blk.h"
#include "hw/lupio/ipi.h"
#include "hw/lupio/pic.h"
#include "hw/lupio/rng.h"
#include "hw/lupio/rtc.h"
#include "hw/lupio/sys.h"
#include "hw/lupio/tmr.h"
#include "hw/lupio/tty.h"
#include "hw/mips/bootloader.h"
#include "hw/mips/cpudevs.h"
#include "hw/qdev-core.h"
#include "hw/qdev-properties.h"
#include "hw/sysbus.h"
#include "qemu/error-report.h"
#include "qemu/module.h"
#include "qemu/typedefs.h"
#include "qemu/units.h"
#include "qom/object.h"
#include "sysemu/device_tree.h"
#include "sysemu/reset.h"
#include "sysemu/sysemu.h"
#include "target/mips/cpu.h"

#include <libfdt.h>

#define TIMEBASE_FREQ 10000000 // 10 MHz

#define MAX_CPUS 8

/* Memory mapping */
enum {
    /* Memory banks */
    MEM_BIOS, MEM_DRAM,
    /* MMIO LupIO devices */
    DEV_BLK, DEV_IPI, DEV_PIC, DEV_RNG,
    DEV_RTC, DEV_SYS, DEV_TMR, DEV_TTY
};

static const struct MemmapEntry {
    hwaddr base;
    hwaddr size;
} memmap[] = {
    /* Memory regions */
    [MEM_DRAM] = { 0x00000000, 0x0 }, /* Size set at runtime, max 256 MiB (0x10000000) */
    [MEM_BIOS] = { 0x1fc00000, 4 * MiB },

    /* MMIO LupIO device regions */
    [DEV_BLK] = { 0x10000000, 0x1000 },
    [DEV_IPI] = { 0x10001000, 0x1000 },
    [DEV_PIC] = { 0x10002000, 0x1000 },
    [DEV_RNG] = { 0x10003000, 0x1000 },
    [DEV_RTC] = { 0x10004000, 0x1000 },
    [DEV_SYS] = { 0x10005000, 0x1000 },
    [DEV_TMR] = { 0x10006000, 0x1000 },
    [DEV_TTY] = { 0x10007000, 0x1000 },
};

/* IRQ mapping */
enum {
    /* Per-cpu IRQs */
    LUPMIPS32_TMR_IRQ = 0,
    LUPMIPS32_IPI_IRQ,
    LUPMIPS32_PCPU_MAX,

    /* Shared IRQs */
    LUPMIPS32_TTY_IRQ = LUPMIPS32_PCPU_MAX * MAX_CPUS,
    LUPMIPS32_BLK_IRQ,
    LUPMIPS32_RNG_IRQ,
};

enum {
    MIPS_HW0 = 2, /* First hardware IRQ in MIPS CPU */
};

/* Dynamic FDT generation */
static void *create_fdt(MachineState *machine)
{
    int fdt_size;
    void *fdt;
    uint32_t *cells;
    int cpu;
    unsigned int smp_cpus = machine->smp.cpus;
    char *nodename;
    uint32_t clk_phandle, cpu_phandle, pic_phandle, sys_phandle;

    fdt = create_device_tree(&fdt_size);
    if (!fdt) {
        error_report("create_device_tree() failed");
        exit(1);
    }

    /* FDT handles */
    cpu_phandle = qemu_fdt_alloc_phandle(fdt);
    clk_phandle = qemu_fdt_alloc_phandle(fdt);
    pic_phandle = qemu_fdt_alloc_phandle(fdt);
    sys_phandle = qemu_fdt_alloc_phandle(fdt);

    /* Root node */
    qemu_fdt_setprop_string(fdt, "/", "model", "LupLab LupMIPS32");
    qemu_fdt_setprop_string(fdt, "/", "compatible", "luplab,lupmips32");
    qemu_fdt_setprop_cell(fdt, "/", "#size-cells", 0x1);
    qemu_fdt_setprop_cell(fdt, "/", "#address-cells", 0x1);

    /* CPUs */
    qemu_fdt_add_subnode(fdt, "/cpus");
    qemu_fdt_setprop_cell(fdt, "/cpus", "timebase-frequency", TIMEBASE_FREQ);
    qemu_fdt_setprop_cell(fdt, "/cpus", "#size-cells", 0x0);
    qemu_fdt_setprop_cell(fdt, "/cpus", "#address-cells", 0x1);

    for (cpu = 0; cpu < smp_cpus; cpu++) {
        nodename = g_strdup_printf("/cpus/cpu@%d", cpu);
        qemu_fdt_add_subnode(fdt, nodename);
        qemu_fdt_setprop_string(fdt, nodename, "compatible", "mips,mips24Kc");
        qemu_fdt_setprop_string(fdt, nodename, "device_type", "cpu");
        qemu_fdt_setprop_string(fdt, nodename, "status", "okay");
        qemu_fdt_setprop_cell(fdt, nodename, "reg", cpu);
        qemu_fdt_setprop_cell(fdt, nodename, "clocks", clk_phandle);
        g_free(nodename);
    }

    /* MIPS CPU interrupt controller */
    nodename = g_strdup_printf("/interrupt-controller");
    qemu_fdt_add_subnode(fdt, nodename);
    qemu_fdt_setprop_string(fdt, nodename, "compatible", "mti,cpu-interrupt-controller");
    qemu_fdt_setprop_cell(fdt, nodename, "#address-cells", 0x0);
    qemu_fdt_setprop(fdt, nodename, "interrupt-controller", NULL, 0);
    qemu_fdt_setprop_cell(fdt, nodename, "#interrupt-cells", 0x1);
    qemu_fdt_setprop_cell(fdt, nodename, "phandle", cpu_phandle);
    g_free(nodename);

    /* MMIO bus for I/O devices (range covers entire address space) */
    qemu_fdt_add_subnode(fdt, "/soc");
    qemu_fdt_setprop(fdt, "/soc", "ranges", NULL, 0);
    qemu_fdt_setprop_string(fdt, "/soc", "compatible", "simple-bus");
    qemu_fdt_setprop_cell(fdt, "/soc", "#size-cells", 0x1);
    qemu_fdt_setprop_cell(fdt, "/soc", "#address-cells", 0x1);

    /* Clock */
    nodename = g_strdup_printf("/lupmips32-clk");
    qemu_fdt_add_subnode(fdt, nodename);
    qemu_fdt_setprop_string(fdt, nodename, "compatible", "fixed-clock");
    qemu_fdt_setprop_cell(fdt, nodename, "#clock-cells", 0x0);
    qemu_fdt_setprop_cell(fdt, nodename, "clock-frequency", TIMEBASE_FREQ);
    qemu_fdt_setprop_cell(fdt, nodename, "phandle", clk_phandle);
    g_free(nodename);

    /* Memory */
    nodename = g_strdup_printf("/memory@%" HWADDR_PRIx, memmap[MEM_DRAM].base);
    qemu_fdt_add_subnode(fdt, nodename);
    qemu_fdt_setprop_cells(fdt, nodename, "reg",
                           memmap[MEM_DRAM].base >> 32, memmap[MEM_DRAM].base,
                           machine->ram_size >> 32, machine->ram_size);
    qemu_fdt_setprop_string(fdt, nodename, "device_type", "memory");
    g_free(nodename);

    /* PIC attached to each CPU's HW0 pin */
    nodename = g_strdup_printf("/soc/lupio-pic@%" HWADDR_PRIx, memmap[DEV_PIC].base);
    qemu_fdt_add_subnode(fdt, nodename);
    qemu_fdt_setprop_string(fdt, nodename, "compatible", "lupio,pic");
    qemu_fdt_setprop_cells(fdt, nodename, "reg",
                           memmap[DEV_PIC].base, memmap[DEV_PIC].size);
    qemu_fdt_setprop(fdt, nodename, "interrupt-controller", NULL, 0);
    qemu_fdt_setprop_cell(fdt, nodename, "#interrupt-cells", 0x1);
    qemu_fdt_setprop_cell(fdt, nodename, "interrupt-parent", cpu_phandle);
    qemu_fdt_setprop_cell(fdt, nodename, "interrupts", MIPS_HW0);
    qemu_fdt_setprop_cell(fdt, nodename, "phandle", pic_phandle);
    g_free(nodename);

    /* TMR attached to PIC's per-cpu IRQs */
    nodename = g_strdup_printf("/soc/lupio-tmr@%" HWADDR_PRIx, memmap[DEV_TMR].base);
    qemu_fdt_add_subnode(fdt, nodename);
    qemu_fdt_setprop_string(fdt, nodename, "compatible", "lupio,tmr");
    qemu_fdt_setprop_cells(fdt, nodename, "reg",
                           memmap[DEV_TMR].base, memmap[DEV_TMR].size);
    qemu_fdt_setprop_cell(fdt, nodename, "clocks", clk_phandle);
    qemu_fdt_setprop_cell(fdt, nodename, "interrupt-parent", pic_phandle);
    cells = g_new0(uint32_t, smp_cpus);
    for (cpu = 0; cpu < smp_cpus; cpu++)
        cells[cpu] = cpu_to_be32(cpu * LUPMIPS32_PCPU_MAX + LUPMIPS32_TMR_IRQ);
    qemu_fdt_setprop(fdt, nodename, "interrupts", cells,
                     smp_cpus * sizeof(uint32_t));
    g_free(cells);
    g_free(nodename);

    /* IPI attached to each CPU's HW2 pin */
    nodename = g_strdup_printf("/soc/lupio-ipi@%" HWADDR_PRIx, memmap[DEV_IPI].base);
    qemu_fdt_add_subnode(fdt, nodename);
    qemu_fdt_setprop_string(fdt, nodename, "compatible", "lupio,ipi");
    qemu_fdt_setprop_cells(fdt, nodename, "reg",
                           memmap[DEV_IPI].base, memmap[DEV_IPI].size);
    /* It's not really an interrupt-controller, but otherwise Linux won't
     * consider it in irqchip_init() */
    qemu_fdt_setprop(fdt, nodename, "interrupt-controller", NULL, 0);
    qemu_fdt_setprop_cell(fdt, nodename, "interrupt-parent", pic_phandle);
    cells = g_new0(uint32_t, smp_cpus);
    for (cpu = 0; cpu < smp_cpus; cpu++)
        cells[cpu] = cpu_to_be32(cpu * LUPMIPS32_PCPU_MAX + LUPMIPS32_IPI_IRQ);
    qemu_fdt_setprop(fdt, nodename, "interrupts", cells,
                     smp_cpus * sizeof(uint32_t));
    g_free(cells);
    g_free(nodename);

    /* System controller */
    nodename = g_strdup_printf("/soc/lupio-sys@%" HWADDR_PRIx, memmap[DEV_SYS].base);
    qemu_fdt_add_subnode(fdt, nodename);
    qemu_fdt_setprop_string(fdt, nodename, "compatible", "syscon");
    qemu_fdt_setprop_cells(fdt, nodename, "reg",
                           memmap[DEV_SYS].base, memmap[DEV_SYS].size);
    qemu_fdt_setprop_cell(fdt, nodename, "phandle", sys_phandle);
    g_free(nodename);

    nodename = g_strdup_printf("/soc/poweroff");
    qemu_fdt_add_subnode(fdt, nodename);
    qemu_fdt_setprop_string(fdt, nodename, "compatible", "syscon-poweroff");
    qemu_fdt_setprop_cell(fdt, nodename, "regmap", sys_phandle);
    qemu_fdt_setprop_cell(fdt, nodename, "offset", 0x0);
    qemu_fdt_setprop_cell(fdt, nodename, "value", 1);
    g_free(nodename);

    nodename = g_strdup_printf("/soc/reboot");
    qemu_fdt_add_subnode(fdt, nodename);
    qemu_fdt_setprop_string(fdt, nodename, "compatible", "syscon-reboot");
    qemu_fdt_setprop_cell(fdt, nodename, "regmap", sys_phandle);
    qemu_fdt_setprop_cell(fdt, nodename, "offset", 0x4);
    qemu_fdt_setprop_cell(fdt, nodename, "value", 1);
    g_free(nodename);

    /* TTY */
    nodename = g_strdup_printf("/soc/lupio-tty@%" HWADDR_PRIx, memmap[DEV_TTY].base);
    qemu_fdt_add_subnode(fdt, nodename);
    qemu_fdt_setprop_string(fdt, nodename, "compatible", "lupio,tty");
    qemu_fdt_setprop_cells(fdt, nodename, "reg",
                           memmap[DEV_TTY].base, memmap[DEV_TTY].size);
    qemu_fdt_setprop_cell(fdt, nodename, "interrupt-parent", pic_phandle);
    qemu_fdt_setprop_cell(fdt, nodename, "interrupts", LUPMIPS32_TTY_IRQ);

    /* Complete chosen node based on TTY's path */
    qemu_fdt_add_subnode(fdt, "/chosen");
    qemu_fdt_setprop_string(fdt, "/chosen", "stdout-path", nodename);
    g_free(nodename);

    /* Block device */
    nodename = g_strdup_printf("/soc/lupio-blk@%" HWADDR_PRIx, memmap[DEV_BLK].base);
    qemu_fdt_add_subnode(fdt, nodename);
    qemu_fdt_setprop_string(fdt, nodename, "compatible", "lupio,blk");
    qemu_fdt_setprop_cells(fdt, nodename, "reg",
                           memmap[DEV_BLK].base, memmap[DEV_BLK].size);
    qemu_fdt_setprop_cell(fdt, nodename, "interrupt-parent", pic_phandle);
    qemu_fdt_setprop_cell(fdt, nodename, "interrupts", LUPMIPS32_BLK_IRQ);
    g_free(nodename);

    /* Real time clock */
    nodename = g_strdup_printf("/soc/lupio-rtc@%" HWADDR_PRIx, memmap[DEV_RTC].base);
    qemu_fdt_add_subnode(fdt, nodename);
    qemu_fdt_setprop_string(fdt, nodename, "compatible", "lupio,rtc");
    qemu_fdt_setprop_cells(fdt, nodename, "reg",
                           memmap[DEV_RTC].base, memmap[DEV_RTC].size);
    g_free(nodename);

    /* Random number generator */
    nodename = g_strdup_printf("/soc/lupio-rng@%" HWADDR_PRIx, memmap[DEV_RNG].base);
    qemu_fdt_add_subnode(fdt, nodename);
    qemu_fdt_setprop_string(fdt, nodename, "compatible", "lupio,rng");
    qemu_fdt_setprop_cells(fdt, nodename, "reg",
                           memmap[DEV_RNG].base, memmap[DEV_RNG].size);
    qemu_fdt_setprop_cell(fdt, nodename, "interrupt-parent", pic_phandle);
    qemu_fdt_setprop_cell(fdt, nodename, "interrupts", LUPMIPS32_RNG_IRQ);
    g_free(nodename);

    return fdt;
}

static void main_cpu_reset(void *opaque)
{
    MIPSCPU *cpu = opaque;
    cpu_reset(CPU(cpu));
}

static void lupmips32_init(MachineState *machine)
{
    Clock *cpuclk;
    MIPSCPU *cpu;
    MemoryRegion *system_memory = get_system_memory();
    MemoryRegion *bios = g_new(MemoryRegion, 1);
    DeviceState *pic;
    int i;
    unsigned int smp_cpus = machine->smp.cpus;
    void *fdt;
    int fdt_size;
    qemu_irq *pic_irqs, *tmr_irqs, *ipi_irqs;

    /*
     * IRQ subsystem
     */
    pic_irqs = g_malloc_n(smp_cpus, sizeof(qemu_irq));
    tmr_irqs = g_malloc_n(smp_cpus, sizeof(qemu_irq));
    ipi_irqs = g_malloc_n(smp_cpus, sizeof(qemu_irq));

    /*
     * CPU subsystem
     */
    cpuclk = clock_new(OBJECT(machine), "cpu-refclk");
    clock_set_hz(cpuclk, TIMEBASE_FREQ);

    for (i = 0; i < smp_cpus; i++) {
        cpu = mips_cpu_create_with_clock(machine->cpu_type, cpuclk);
        cpu_mips_irq_init_cpu(cpu);
        cpu_mips_clock_init(cpu);
        qemu_register_reset(main_cpu_reset, cpu);

        /* Each CPU's HW0 IRQ input is connected to the PIC */
        pic_irqs[i] = cpu->env.irq[MIPS_HW0];
    }

    /*
     * Memory subsystem
     */
    if (machine->ram_size > 256 * MiB) {
        error_report("RAM size more than 256 MiB is not supported");
        exit(EXIT_FAILURE);
    }

    /* Main memory */
    memory_region_add_subregion(system_memory, memmap[MEM_DRAM].base, machine->ram);

    /* BIOS memory */
    memory_region_init_rom(bios, NULL, "lupmips32.bios",
                           memmap[MEM_BIOS].size, &error_fatal);
    memory_region_add_subregion(system_memory, memmap[MEM_BIOS].base, bios);

    /*
     * MMIO subsystem
     */

    /* Core devices */
    pic = lupio_pic_create(memmap[DEV_PIC].base, smp_cpus, pic_irqs);

    for (i = 0; i < smp_cpus; i++) {
        tmr_irqs[i] = qdev_get_gpio_in(DEVICE(pic),
                                       i * LUPMIPS32_PCPU_MAX + LUPMIPS32_TMR_IRQ);
        ipi_irqs[i] = qdev_get_gpio_in(DEVICE(pic),
                                       i * LUPMIPS32_PCPU_MAX + LUPMIPS32_IPI_IRQ);
    }

    lupio_tmr_create(memmap[DEV_TMR].base, smp_cpus, tmr_irqs, TIMEBASE_FREQ);

    lupio_ipi_create(memmap[DEV_IPI].base, smp_cpus, ipi_irqs);

    g_free(pic_irqs);
    g_free(tmr_irqs);
    g_free(ipi_irqs);

    /* Peripheral devices */
    lupio_blk_create(memmap[DEV_BLK].base,
                     qdev_get_gpio_in(DEVICE(pic), LUPMIPS32_BLK_IRQ),
                     drive_get(IF_NONE, 0, 0));

    lupio_rtc_create(memmap[DEV_RTC].base);

    lupio_rng_create(memmap[DEV_RNG].base,
                     qdev_get_gpio_in(DEVICE(pic), LUPMIPS32_RNG_IRQ));

    lupio_sys_create(memmap[DEV_SYS].base);

    lupio_tty_create(memmap[DEV_TTY].base,
                     qdev_get_gpio_in(DEVICE(pic), LUPMIPS32_TTY_IRQ),
                     serial_hd(0));

    /*
     * FDT generation
     */
    fdt = create_fdt(machine);
    if (machine->kernel_cmdline)
        qemu_fdt_setprop_string(fdt, "/chosen", "bootargs",
                                machine->kernel_cmdline);

    fdt_size = fdt_totalsize(fdt);
    if (fdt_size <= 0) {
        error_report("invalid device-tree");
        exit(1);
    }

    g_assert(fdt_pack(fdt) == 0);

    /* Dump DTB if requested */
    qemu_fdt_dumpdtb(fdt, fdt_size);

    /*
     * Software loading
     */
    if (machine->kernel_filename) {
        uint64_t kernel_entry, kernel_high;
        ssize_t kernel_size;

        hwaddr dtb_paddr, dtb_vaddr;

        uint32_t *bios_ptr;

        /* Load kernel */
        kernel_size = load_elf(machine->kernel_filename, NULL,
                           cpu_mips_kseg0_to_phys, NULL,
                           &kernel_entry, NULL, &kernel_high,
                           NULL, 0, EM_MIPS, 1, 0);

        if (kernel_size <= 0) {
            error_report("unable to load kernel image");
            exit(1);
        }

        /* Load FDT just above kernel, aligned on 64KiB boundary */
        dtb_paddr = QEMU_ALIGN_UP(kernel_high, 64 * KiB);
        dtb_vaddr = cpu_mips_phys_to_kseg0(NULL, dtb_paddr);

        rom_add_blob_fixed("dtb", fdt, fdt_size, dtb_paddr);

        /*
         * Generate BIOS code
         *
         * Single jump to kernel entry with a0 == -2 (meaning using a device
         * tree) and a1 == virtual address of device tree
         */
        bios_ptr = memory_region_get_ram_ptr(bios);
        bl_gen_jump_kernel(&bios_ptr, 0,
                           (int32_t)-2, dtb_vaddr, 0, 0, kernel_entry);
    } else {
        error_report("Please provide a -kernel argument");
        exit(1);
    }
}

static void lupmips32_machine_init(MachineClass *mc)
{
    mc->desc = "LupMIPS32 (MIPS32+LupIO) board";
    mc->init = lupmips32_init;
    mc->max_cpus = MAX_CPUS;
    mc->default_cpu_type = MIPS_CPU_TYPE_NAME("24Kf");
    mc->default_ram_id = "lupmips32.ram";
}

DEFINE_MACHINE("lupmips32", lupmips32_machine_init)
